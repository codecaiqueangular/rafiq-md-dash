import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import {
  PerfectScrollbarModule,
  PERFECT_SCROLLBAR_CONFIG,
  PerfectScrollbarConfigInterface
} from 'ngx-perfect-scrollbar';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { AppRoutingModule } from "./app-routing.module";
import { SharedModule } from "./shared/shared.module";
import { ToastrModule } from "ngx-toastr";
import { AgmCoreModule } from "@agm/core";
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { StoreModule } from "@ngrx/store";
import { AppComponent } from "./app.component";
import { ContentLayoutComponent } from "./layouts/content/content-layout.component";
import { FullLayoutComponent } from "./layouts/full/full-layout.component";
import { DragulaService } from "ng2-dragula";
import { AuthGuard } from "./shared/auth/auth-guard.service";
import { FormsModule } from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import { environment } from '../environments/environment';
import { ProfileComponent  } from './components/profile/profile.component';
import { ReactiveFormsModule } from "@angular/forms";
import { LoginComponent } from './components/login/login.component';
import { UsersComponent } from './components/users/users.component';
import { TermsConditioonsComponent } from './components/terms-conditioons/terms-conditioons.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { SettingComponent } from './components/setting/setting.component';
import { CityComponent } from './components/city/city.component';
import { CookieService } from 'ngx-cookie-service';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { MangerAcountComponent } from './components/manger-acount/manger-acount.component';
import { TestsComponent } from './components/tests/tests.component';
import { SpecialistsComponent } from './components/specialists/specialists.component';
import { QuestionComponent } from './components/question/question.component';
import { AnswersComponent } from './components/answers/answers.component';
import { SpectialTypeComponent } from './components/spectial-type/spectial-type.component';
import { SpectialQuestionComponent } from './components/spectial-question/spectial-question.component';
import { ReservationsComponent } from './components/reservations/reservations.component';
import { LanguageComponent } from './components/language/language.component';
import { CashComponent } from './components/cash/cash.component';
import { ConditionsComponent } from './components/conditions/conditions.component';
import { NewsComponent } from './components/news/news.component';
import { InstitutionsComponent } from './components/institutions/institutions.component';
import { PreviousDealingsComponent } from './components/previous-dealings/previous-dealings.component';
import { CoponsComponent } from './components/copons/copons.component';
import { AdminsComponent } from './components/admins/admins.component';
import { StudentsComponent } from './components/users/students/students.component';
import { WlyAlamerComponent } from './components/users/wly-alamer/wly-alamer.component';
import { TrainerComponent } from './components/users/trainer/trainer.component';
import { ConfirmResComponent } from './components/reservations/confirm-res/confirm-res.component';
import { DashComponent } from './components/dash/dash.component';
import { ChartistModule } from 'ng-chartist';
import { BlogComponent } from './components/blog/blog.component';
import { CategoryComponent } from './components/blog/category/category.component';
import { OfferComponent } from './components/offer/offer.component';
import { HomeComponent } from './components/home/home.component';
import { SafePipe } from './shared/pipes/SafePipe.pipe';
import { BrowserModule } from '@angular/platform-browser';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NotificationsComponent } from 'app/components/notifications/notifications.component';


 
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
  wheelPropagation: false
};

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}

@NgModule({
  declarations: [AppComponent, FullLayoutComponent, ContentLayoutComponent,ProfileComponent ,
     LoginComponent, UsersComponent, TermsConditioonsComponent, ContactUsComponent, SettingComponent,
       CityComponent,
       SafePipe, NotificationsComponent,
       AboutUsComponent, MangerAcountComponent, TestsComponent, 
       SpecialistsComponent, QuestionComponent, AnswersComponent, SpectialTypeComponent, 
       SpectialQuestionComponent, ReservationsComponent, LanguageComponent, CashComponent,
        ConditionsComponent, NewsComponent, InstitutionsComponent, PreviousDealingsComponent, 
        CoponsComponent, AdminsComponent, StudentsComponent, WlyAlamerComponent, TrainerComponent, 
         ConfirmResComponent, DashComponent, BlogComponent, CategoryComponent, OfferComponent, HomeComponent],

  imports: [
  
    BrowserAnimationsModule,
    BrowserModule,
   
    StoreModule.forRoot({}),
    NgxPaginationModule,
    NgMultiSelectDropDownModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    AppRoutingModule,
    SharedModule,
    ChartistModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    NgbModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    AgmCoreModule.forRoot({
      apiKey: "YOUR KEY"
    }),
    PerfectScrollbarModule
  ],
  providers: [

    CookieService,
    AuthGuard,
    DragulaService,
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    { provide: PERFECT_SCROLLBAR_CONFIG, useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
