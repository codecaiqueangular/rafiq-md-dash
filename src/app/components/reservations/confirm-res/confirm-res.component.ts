import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../../shared/services/api.service';
import swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-confirm-res',
  templateUrl: './confirm-res.component.html',
  styleUrls: ['./confirm-res.component.scss']
})
export class ConfirmResComponent implements OnInit {

  constructor(public toastr: ToastrService,
    private ApiService: ApiService,
     private cookieService: CookieService, private router: Router, private route: ActivatedRoute) {

   
  }


  currentPage: string = "list";
  p: number = 1;
  taskid: string;

  
  imgSrc: string ="assets/img/image-placeholder.jpg";
  selectedImage: any = null;
  isSubmitted: boolean;
  branch:any[];

  

  Userform = new FormGroup({
    cash: new FormControl('', Validators.required),
    user_id: new FormControl('',),
   
  })


  ngOnInit() {

    this.ApiService.show_reservationshistory().subscribe(
      res => {
        let resources: any[] = res["data"];
        this.branch = resources;
        

      });


    this.resetForm();

  }

  

   
  onSubmit(formValue) {
    if (this.Userform.valid) {


      formValue["user_id"]=this.taskid;
      this.ApiService.recharge_wallet(formValue).subscribe(
        res => {
  
          let message: any = res["message"];

            this.toastr.success(' ', message);
            this.resetForm();
            this.ngOnInit();
            this.currentPage = "list";

          });
       
      
    } else {
      this.toastr.warning('Please fill out all fields correctly', '');

    }


  }


  wallet(page,id) {
    this.currentPage = page;
    this.taskid = id;

   }
 


  get formControls() {
    return this.Userform['controls'];
  }



  showPreview(event: any) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => this.imgSrc = e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.selectedImage = event.target.files[0];
    }
    else {
      this.imgSrc ="assets/img/image-placeholder.jpg" ;
      
      this.selectedImage = null;
    }
  }


  resetForm() {
    this.Userform.reset();
   
  }


  showPage(page: string) {
    this.currentPage = page;
    this.ngOnInit()
  }

  deleteTask(id) {

  
     swal.fire({
       title: 'Are you sure?',
       type: 'warning',
       showCancelButton: true,
       confirmButtonColor: '#3085d6',
       cancelButtonColor: '#d33',
       confirmButtonText: 'Yes, block it!'
     }).then((result) => {
       if (result.value) {

       this.ApiService.block_user(id).subscribe(
         res => {
  
           this.ngOnInit();
         });
  

         swal.fire(
           'block!',
           'Your use has been block.',
           'success'

           
         )
       }
     })
 
   
  }


  


}
