import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmResComponent } from './confirm-res.component';

describe('ConfirmResComponent', () => {
  let component: ConfirmResComponent;
  let fixture: ComponentFixture<ConfirmResComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmResComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmResComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
