import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../shared/services/api.service';
import swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';
import { getgid } from 'process';


@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.scss']
})
export class ReservationsComponent implements OnInit {

  constructor(public toastr: ToastrService,
    private ApiService: ApiService,
     private cookieService: CookieService, private router: Router, private route: ActivatedRoute) {

   
   
      this.role = localStorage.getItem('rol');
      //console.log(this.role);
      this.all = localStorage.getItem('rol').includes("show_allreservations");
      this.add = localStorage.getItem('rol').includes("insert_institutions");
      this.delete = localStorage.getItem('rol').includes("insert_institutions");
      this.update = localStorage.getItem('rol').includes("update_institutions");
      this.show_acceptedreservations =  localStorage.getItem('rol').includes("show_acceptedreservations");
      this.show_canceledreservations =  localStorage.getItem('rol').includes("show_canceledreservations");

    
      this.show_completereservations =  localStorage.getItem('rol').includes("show_completereservations");
 /*  this.access = localStorage.getItem('rol').includes('اضافة صلاحيات'); */
     //  console.log( this.role)
     console.log(this.all)
     console.log(this.add)
    }
   all
   show_completereservations
   show_acceptedreservations
   show_canceledreservations
   add
   delete
   update
   access

   role



  currentPage: string = "list";
  p: number = 1;
  pp: number = 1;
  ppp: number = 1;
  pppp: number = 1;
  taskid: string;
  imageUrl = environment.imageUrl;

  imgSrc: string ="assets/img/image-placeholder.jpg";
  selectedImage: any = null;
  isSubmitted: boolean;
  branch:any[];

  Userform = new FormGroup({
    date_from: new FormControl('', Validators.required),
    date_to: new FormControl('', Validators.required),
  })

  Userform1 = new FormGroup({
    date_from: new FormControl('', Validators.required),
    date_to: new FormControl('', Validators.required),
  })

  Userform2= new FormGroup({
    date_from: new FormControl('', Validators.required),
    date_to: new FormControl('', Validators.required),
  })


  Userform3 = new FormGroup({
    date_from: new FormControl('', Validators.required),
    date_to: new FormControl('', Validators.required),
  })


  Userform4 = new FormGroup({
    date_from: new FormControl('', Validators.required),
    date_to: new FormControl('', Validators.required),
  })

  accepted
canceled
complete


  ngOnInit() {

    this.ApiService.show_allreservations().subscribe(
      res => {
        let resources: any[] = res["data"];
           console.log(res["data"])
        this.branch = resources;
        console.log(this.branch)
       // this.branch.filter( data => data.state == "wait to confirm"  )
      /*    for (let index = 0; index > res["data"].length; index++) {
       
          if(res["data"][index].state == "wait to confirm"  || res["data"][index].state == "في انتظار تأكيد الحجز"){
            this.branch.push(res["data"][index])
          }

        }  */

        this.copy_of_branch = []

        for(let i =0 ; i<this.branch.length ; i++){
          if(this.branch[i].state === "wait to confirm" ){
            this.copy_of_branch.push(this.branch[i])
          }
        }
        this.branch = this.copy_of_branch
     //   this.copy_of_branch  = this.branch
      });

      
    this.ApiService.show_acceptedreservations().subscribe(
      res => {
        let resources: any[] = res["data"];
        this.accepted = resources;
        this.copy_of_accepted  = this.accepted
      });
      
    this.ApiService.show_canceledreservations().subscribe(
      res => {
        let resources: any[] = res["data"];
        this.canceled = resources;
        this.copy_of_canceled  = this.canceled
      });
      
    this.ApiService.show_reservationshistory().subscribe(
      res => {
        let resources: any[] = res["data"];
        this.complete = resources;
     this.copy_of_complete  = this.complete

      });

    this.resetForm();


  
     
    
  }

  



  copy_of_complete = []
	filter_by_date_complete(formvalue){

		 if(formvalue.date_from != '' && formvalue.date_to != ''){
		console.log(formvalue)
		this.copy_of_complete = []
		let date_to = formvalue.date_to;
     let date_from = formvalue.date_from;
     console.log(this.complete)
		 	for (let index = 0; index < this.complete.length; index++) {
        console.log(this.complete)
		 	if(this.complete[index].date >= date_from &&   this.complete[index].date <= date_to ){
				//this.orders = [];
				this.copy_of_complete.push(this.complete[index]);
			console.log(this.complete)
			} 
		}
		 }else{
			this.copy_of_complete = this.complete
		 }
	
		
	}


  copy_of_canceled = []
	filter_by_date_canceled(formvalue){

		 if(formvalue.date_from != '' && formvalue.date_to != ''){
		console.log(formvalue)
		this.copy_of_canceled = []
		let date_to = formvalue.date_to;
     let date_from = formvalue.date_from;
     console.log(this.canceled)
		 	for (let index = 0; index < this.canceled.length; index++) {
        console.log(this.canceled)
		 	if(this.canceled[index].date >= date_from &&   this.canceled[index].date <= date_to ){
				//this.orders = [];
				this.copy_of_canceled.push(this.canceled[index]);
			console.log(this.canceled)
			} 
		}
		 }else{
			this.copy_of_canceled = this.canceled
		 }
	
		
	}




  copy_of_accepted = []
	filter_by_date_accepted(formvalue){

		 if(formvalue.date_from != '' && formvalue.date_to != ''){
		console.log(formvalue)
		this.copy_of_accepted = []
		let date_to = formvalue.date_to;
     let date_from = formvalue.date_from;
     console.log(this.accepted)
		 	for (let index = 0; index < this.accepted.length; index++) {
        console.log(this.accepted)
		 	if(this.accepted[index].date >= date_from &&   this.accepted[index].date <= date_to ){
			
				this.copy_of_accepted.push(this.accepted[index]);
			console.log(this.accepted)
			} 
		}
		 }else{
			this.copy_of_accepted = this.accepted
		 }
	
		
  }
  


  copy_of_branch = []
	filter_by_date_branch(formvalue){

		 if(formvalue.date_from != '' && formvalue.date_to != ''){
		console.log(formvalue)
		this.copy_of_branch = []
		let date_to = formvalue.date_to;
     let date_from = formvalue.date_from;
     console.log(this.branch)
		 	for (let index = 0; index < this.branch.length; index++) {
        console.log(this.branch)
		 	if(this.branch[index].date >= date_from &&   this.branch[index].date <= date_to ){
			
				this.copy_of_branch.push(this.branch[index]);
			console.log(this.branch)
			} 
		}
		 }else{
			this.copy_of_branch = this.branch
		 }
	
		
	}

 



   
  onSubmit(formValue) {
    if (this.Userform.valid) {


      formValue["user_id"]=this.taskid;
      this.ApiService.recharge_wallet(formValue).subscribe(
        res => {
  
          let message: any = res["message"];

            this.toastr.success(' ', message);
            this.resetForm();
            this.ngOnInit();
            this.currentPage = "list";

          });
       
      
    } else {
      this.toastr.warning('Please fill out all fields correctly', '');

    }


  }


  wallet(page,id) {
    this.currentPage = page;
    this.taskid = id;

   }
 


  get formControls() {
    return this.Userform1['controls'];
  }



  showPreview(event: any) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => this.imgSrc = e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.selectedImage = event.target.files[0];
    }
    else {
      this.imgSrc ="assets/img/image-placeholder.jpg" ;
      
      this.selectedImage = null;
    }
  }


  resetForm() {
    this.Userform1.reset();
    this.Userform2.reset();
    this.Userform3.reset();
    this.Userform4.reset();
   
  }


  showPage(page: string) {
    this.currentPage = page;
    this.ngOnInit()
  }

  deleteTask(id) {

  
     swal.fire({
       title: 'Are you sure?',
       type: 'warning',
       showCancelButton: true,
       confirmButtonColor: '#3085d6',
       cancelButtonColor: '#d33',
       confirmButtonText: 'Yes, block it!'
     }).then((result) => {
       if (result.value) {

       this.ApiService.block_user(id).subscribe(
         res => {
  
           this.ngOnInit();
         });
  

         swal.fire(
           'block!',
           'The reservation has been blocked.',
           'success'

           
         )
       }
     })
 
   
  }




  acceptTask(id) {

  
    swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Accept it!'
    }).then((result) => {
      if (result.value) {

      this.ApiService.acceptReservation(id).subscribe(
        res => {
 
          this.ngOnInit();
          
           swal.fire(
          'Accept!',
          'The reservation has been Accepted.',
          'success'

          
        )
        });
 

      }
    })

  
 }

  


}
