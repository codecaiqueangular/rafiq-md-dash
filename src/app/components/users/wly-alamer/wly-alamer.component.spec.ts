import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WlyAlamerComponent } from './wly-alamer.component';

describe('WlyAlamerComponent', () => {
  let component: WlyAlamerComponent;
  let fixture: ComponentFixture<WlyAlamerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WlyAlamerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WlyAlamerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
