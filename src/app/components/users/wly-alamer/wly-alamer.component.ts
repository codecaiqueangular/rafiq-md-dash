import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../../shared/services/api.service';
import swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-wly-alamer',
  templateUrl: './wly-alamer.component.html',
  styleUrls: ['./wly-alamer.component.scss']
})
export class WlyAlamerComponent implements OnInit {


  constructor(public toastr: ToastrService,
    private ApiService: ApiService,
     private cookieService: CookieService, private router: Router, private route: ActivatedRoute) {

   
      this.role = localStorage.getItem('rol');
      //console.log(this.role);
     this.all = localStorage.getItem('rol').includes("show_walyalamer");
    this.add = localStorage.getItem('rol').includes("ubblock_user");
      this.delete = localStorage.getItem('rol').includes("block_user");
 /*      this.update = localStorage.getItem('rol').includes("تعديل موظف ");
       this.access = localStorage.getItem('rol').includes('اضافة صلاحيات'); */
     //  console.log( this.role)
     console.log(this.all)
     console.log(this.add)
    }
   all
   add
   delete
   
   update
   access
   role



  currentPage: string = "list";
  p: number = 1;
  taskid: string;
  imageUrl = environment.imageUrl;

  imgSrc: string ="assets/img/image-placeholder.jpg";
  selectedImage: any = null;
  isSubmitted: boolean;
  show_students:any[];
  show_trainer:any[];
  show_wlyAlamer:any[];
  show_blockedstudent:any[];
  show_blockedwlyamer:any[];
  show_blockedtrainer:any[];

  Userform = new FormGroup({
    cash: new FormControl('', Validators.required),
    user_id: new FormControl('',),
   
  })


  Userformx = new FormGroup({
    comission: new FormControl('', Validators.required),
    user_id: new FormControl('',),
   
  })



  

  /*********** filter by name ***********************/
  AllUsers2
  AllUsers4
  AllUsers3
  _listFilter = '';
  filteredAllDiscussion: any[] = [];
  get listFilter(): string {
    return this._listFilter;
  }
 

  filterbyName=[]
  search(filterBye){
    this.show_trainer = this.AllUsers2
    this.filterbyName =[]
    for (let index = 0; index < this.show_trainer.length; index++) {
       

      
      if((this.show_trainer[index].first_name != null&& this.show_trainer[index].first_name.includes(filterBye)) || 
      (this.show_trainer[index].last_name != null && this.show_trainer[index].last_name.toLowerCase().includes(filterBye.toLowerCase()))
      || 
      (this.show_trainer[index].phone != null && this.show_trainer[index].phone.toLowerCase().includes(filterBye.toLowerCase()))
      || 
      (this.show_trainer[index].email != null && this.show_trainer[index].email.toLowerCase().includes(filterBye.toLowerCase()))
      ){

      
        this.filterbyName.push(this.show_trainer[index])
      }
      
    }
   this.show_trainer = this.filterbyName
  }


  filterbyName2=[]
  search2(filterBye){
    this.show_wlyAlamer = this.AllUsers3
    this.filterbyName2 =[]
    for (let index = 0; index < this.show_wlyAlamer.length; index++) {
   

      
      if((this.show_wlyAlamer[index].first_name != null&& this.show_wlyAlamer[index].first_name.includes(filterBye)) || 
      (this.show_wlyAlamer[index].last_name != null && this.show_wlyAlamer[index].last_name.toLowerCase().includes(filterBye.toLowerCase()))
      || 
      (this.show_wlyAlamer[index].phone != null && this.show_wlyAlamer[index].phone.toLowerCase().includes(filterBye.toLowerCase()))
      || 
      (this.show_wlyAlamer[index].email != null && this.show_wlyAlamer[index].email.toLowerCase().includes(filterBye.toLowerCase()))
      ){
        this.filterbyName2.push(this.show_wlyAlamer[index])
      }
      
    }
   this.show_wlyAlamer = this.filterbyName2
  }


  filterbyName3=[]
  search3(filterBye){
    this.show_blockedwlyamer = this.AllUsers4
    this.filterbyName3 =[]
    for (let index = 0; index < this.show_blockedwlyamer.length; index++) {
      

      
      if((this.show_blockedwlyamer[index].first_name != null&& this.show_blockedwlyamer[index].first_name.includes(filterBye)) || 
      (this.show_blockedwlyamer[index].last_name != null && this.show_blockedwlyamer[index].last_name.toLowerCase().includes(filterBye.toLowerCase()))
      || 
      (this.show_blockedwlyamer[index].phone != null && this.show_blockedwlyamer[index].phone.toLowerCase().includes(filterBye.toLowerCase()))
      || 
      (this.show_blockedwlyamer[index].email != null && this.show_blockedwlyamer[index].email.toLowerCase().includes(filterBye.toLowerCase()))
      ){
        this.filterbyName3.push(this.show_blockedwlyamer[index])
      }
      
    }
   this.show_blockedwlyamer = this.filterbyName3
  }

  /*********** filter by name ***********************/


  ngOnInit() {


    this.ApiService.show_wlyAlamer().subscribe(
      res => {
        let resources: any[] = res["data"];
        this.show_wlyAlamer = resources;
        this.AllUsers3 = resources;
      });

    this.ApiService.show_blockedwlyamer().subscribe(
      res => {
        let resources: any[] = res["data"];
        this.show_blockedwlyamer = resources;
        this.AllUsers4 = resources;

      });


    this.resetForm();

  }

  
  
   
  comission(formValue) {

    if (this.Userformx.valid) {
      formValue["user_id"]=this.taskid;
      this.ApiService.update_commission(formValue).subscribe(
        res => {
  
          let message: any = res["message"];

            this.toastr.success(' ', message);
            this.resetForm();
            this.ngOnInit();
            this.currentPage = "list";

          });
       
      
    } else {
      this.toastr.warning('Please fill out all fields correctly', '');

    }


  }
  onSubmit(formValue) {
    if (this.Userform.valid) {


      formValue["user_id"]=this.taskid;
      this.ApiService.recharge_wallet(formValue).subscribe(
        res => {
  
          let message: any = res["message"];

            this.toastr.success(' ', message);
            this.resetForm();
            this.ngOnInit();
            this.currentPage = "list";

          });
       
      
    } else {
      this.toastr.warning('Please fill out all fields correctly', '');

    }


  }


  wallet(page,id) {
    this.currentPage = page;
    this.taskid = id;

   }
 


  get formControls() {
    return this.Userform['controls'];
  }



  showPreview(event: any) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => this.imgSrc = e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.selectedImage = event.target.files[0];
    }
    else {
      this.imgSrc ="assets/img/image-placeholder.jpg" ;
      
      this.selectedImage = null;
    }
  }


  resetForm() {
    this.Userform.reset();
   
  }


  showPage(page: string) {
    this.currentPage = page;
    this.ngOnInit()
  }

  deleteTask(id) {

  
     swal.fire({
       title: 'Are you sure?',
       type: 'warning',
       showCancelButton: true,
       confirmButtonColor: '#3085d6',
       cancelButtonColor: '#d33',
       confirmButtonText: 'Yes, block it!'
     }).then((result) => {
       if (result.value) {

       this.ApiService.block_user(id).subscribe(
         res => {
  
           this.ngOnInit();
         });
  

         swal.fire(
           'block!',
           'Your use has been block.',
           'success'

           
         )
       }
     })
 
   
  }


  unblock_user(id) {

  
    swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Unblock it!'
    }).then((result) => {
      if (result.value) {

      this.ApiService.unblock_user(id).subscribe(
        res => {
 
          this.ngOnInit();
        });
 

        swal.fire(
          'Un Blocked!',
          'Your use has been UnBlocked.',
          'success'

          
        )
      }
    })

  
 }


}
