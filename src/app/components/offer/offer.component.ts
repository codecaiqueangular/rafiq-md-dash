import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../shared/services/api.service';
import swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.scss']
})
export class OfferComponent implements OnInit {

 

  constructor(public toastr: ToastrService, 
    private ApiService: ApiService, private cookieService: CookieService,
    private router: Router, private route: ActivatedRoute) {
  }

  public loading = false;

  currentPage: string = "list";
  p: number = 1;
  taskid: string;
  imgSrc: string = "assets/img/image-placeholder.jpg";
  selectedImage: any = null;
  isSubmitted: boolean;
  usersarr: any[];
  branch: any[];
  branchD: any[];
  imageUrl = environment.imageUrl;

  Userform = new FormGroup({
    image: new FormControl('',),
    text: new FormControl('', Validators.required),
   // type: new FormControl('', Validators.required),
    link: new FormControl('', Validators.required),

  })

  id 
  text
  image
  link
  type

  ngOnInit() {

    this.ApiService.show_dashE3lan().subscribe(
      res => {

        let resources: any[] = res["data"];
        this.branch = resources;

        console.log(resources)

        this.id = res["data"].id
        this.text= res["data"].text
        this.image= res["data"].image
        this.link= res["data"].link
     //   this.type= res["data"].type
        console.log(this.image)
      });
      this.resetForm();

  }


  onSubmit(formValue) {

    if (this.Userform.valid) {

      this.ApiService.insert_specialists(formValue,this.selectedImage).subscribe(
        res => {
  
            this.toastr.success(' ', 'insert successfully');
            this.resetForm();
            this.ngOnInit();
            this.currentPage = "list";

          });
          
      
    } else {

      this.toastr.warning('Please fill out all fields correctly', '');

    }

  }

  onEdit(formValue) {

    if (this.Userform.valid) {

     this.ApiService.update_E3lan(formValue,this.selectedImage).subscribe(
       res => {
  
       
           this.toastr.success(' ', 'Update  successfully');
           this.resetForm();
           this.ngOnInit();
           this.currentPage = "list";

         });
      
      
    } else {
      this.toastr.warning('Please fill out all fields correctly', '');

    }

  }


  showPreview(event: any) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => this.imgSrc = e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.selectedImage = event.target.files[0];
    }
    else {
      this.imgSrc ="assets/img/image-placeholder.jpg" ;
      
      this.selectedImage = null;
    }
  }
  

  get formControls() {
    return this.Userform['controls'];
  }

  
  resetForm() {
    this.Userform.reset();
    this.imgSrc = "assets/img/image-placeholder.jpg";
  }


  showPage(page: string) {
    this.currentPage = page;
    this.resetForm()

    this.ngOnInit()
  }


  deleteTask(id) {

    swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {

       this.ApiService.delete_specialist(id).subscribe(
         res => {

           this.ngOnInit();
         });



        swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'


        )
      }
    })



  }


  updatetask(page) {


    this.currentPage = page;
    





       this.Userform.setValue({
        image: '',
/*           name: this.branchD['name'],
        E_name: this.branchD['E_name'], */

        text:this.text,
     
        link:this.link,
       // type:this.type

       });

       this.imgSrc =this.imageUrl+ "/how_work//" + this.image;

  }
 

 
  

}
