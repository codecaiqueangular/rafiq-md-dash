import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../shared/services/api.service';
import swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-admins',
  templateUrl: './admins.component.html',
  styleUrls: ['./admins.component.scss']
})
export class AdminsComponent implements OnInit {

 
  constructor(public toastr: ToastrService,
    private ApiService: ApiService,
     private cookieService: CookieService, private router: Router, private route: ActivatedRoute) {

      this.role = localStorage.getItem('rol');

       console.log(this.role);
      this.all = this.role.includes("show_admins");
      this.add = this.role.includes("insert_admins");
      this.delete = this.role.includes("delete_admins");
      this.update = this.role.includes("update_admins");
 /*  this.access = this.cookieService.get('rol').includes('اضافة صلاحيات'); */
     //  console.log( this.role)
     console.log(this.all)
     console.log(this.add)
    }
   all
   add
   delete
   update
   access

   role

  currentPage: string = "list";
  p: number = 1;
  taskid: string;
  imageUrl = environment.imageUrl;

  imgSrc: string ="assets/img/image-placeholder.jpg";
  selectedImage: any = null;
  isSubmitted: boolean;
  usersarr:any[];
  show_Adminroles:any[];
  city:any[];
  
  Userform = new FormGroup({
    
    first_name: new FormControl('', Validators.required),
    last_name: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),  
    phone: new FormControl('', Validators.required),  
    passwords: new FormControl('', Validators.required),
   // state_id: new FormControl('', Validators.required),

  })


  
  
  /*********** filter by name ***********************/
  AllUsers2
  AllUsers4
  AllUsers3
  _listFilter = '';
  filteredAllDiscussion: any[] = [];
  get listFilter(): string {
    return this._listFilter;
  }
 
 

  filterbyName2=[]
  search2(filterBye){
    this.usersarr = this.AllUsers3
    this.filterbyName2 =[]
    for (let index = 0; index < this.usersarr.length; index++) {
 
      
      if(this.usersarr[index].first_name != null && this.usersarr[index].first_name.toLowerCase().includes(filterBye.toLowerCase())
      || 
      (this.usersarr[index].last_name != null && this.usersarr[index].last_name.toLowerCase().includes(filterBye.toLowerCase()))
      || 
      (this.usersarr[index].phone != null && this.usersarr[index].phone.toLowerCase().includes(filterBye.toLowerCase()))
      || 
      (this.usersarr[index].state != null && this.usersarr[index].state.toLowerCase().includes(filterBye.toLowerCase()))
      || 
      (this.usersarr[index].email != null && this.usersarr[index].email.toLowerCase().includes(filterBye.toLowerCase()))
      ){
        this.filterbyName2.push(this.usersarr[index])
      }
      
    }
   this.usersarr = this.filterbyName2
  }


 

  /*********** filter by name ***********************/



  Userform2 = new FormGroup({
    
    role_id: new FormControl('', Validators.required),
   /*  last_name: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),  
    phone: new FormControl('', Validators.required),  
    passwords: new FormControl('', Validators.required),
    state_id: new FormControl('', Validators.required), */

  })

  ngOnInit() {

    
    this.ApiService.show_admins().subscribe(
      res => {

        let resources: any[] = res["data"];
        this.usersarr=resources;
        this.AllUsers3=resources;

      });

      this.ApiService.show_Adminroles().subscribe(
        res => {
  
          let resources: any[] = res["data"];
          this.show_Adminroles=resources;
          
        });
        this.Userform.reset();
        this.Userform2.reset();
        
      
  }




  onSubmit2(formValue) {
 

    console.log(formValue)
    if (this.Userform2.valid) {
 

      this.taskid
 
      
      this.ApiService.add_adminrole(formValue,this.taskid).subscribe(
        res => {

         this.toastr.success(' ', 'تم الاضافة بنجاح');
         this.resetForm();
         this.ngOnInit();
         this.currentPage = "list";  

       });
    
      }else{
        this.toastr.warning('من فضلك املء جميع الحقول', '');

      }
 

  }













  area

  shpwcategory(page, id) {


    this.currentPage = page;
     this.taskid = id;


     this.ApiService.show_alladminRoles(id).subscribe(
      /* res => {

        let resources: any[] = res["data"];
        this.branchD = resources
          console.log(resources);
         this.Userform.setValue({
          name: this.branchD['name'],
          E_name: this.branchD['E_name'],
          image:this.branchD['image']
         
        
          

          
        }); 

        this.imgSrc=this.imageUrl+'/shope_category/'+this.branchD['image']
      this.first_name= this.branchD['name']
      this.E_name= this.branchD['E_name'] */

      res => {

        let resources: any = res["data"];
        this.area = resources
     
       /*  
        this.Userform.setValue({
         image: '',
         title: resources.title,
        // E_name: resources.E_name,
         */

        });

       // this.imgSrc =resources.image;

        
      
      
  }



  roles
  showPage2(page: string) {
    this.currentPage = page;
    this.ngOnInit();
    this.ApiService.show_Adminroles().subscribe(
      res => {

        let resources: any[] = res["data"];
 
        this.roles=resources;

     });
  }

  onSubmit(formValue) {

    console.log(formValue)
 
     if (this.Userform.valid) {
 
     
     this.ApiService.add_admin(formValue).subscribe(
       res => {
        let error: any = res["error"];
        let message: any = res["message"];
        if(error==4){
          this.toastr.warning(message, '');

        }else if(error==3){
          this.toastr.warning(message, '');

        }else if(error==0){
          this.toastr.success(' ', message);
          this.resetForm();
          this.ngOnInit();
          this.currentPage = "list";
        }else{
          this.toastr.warning(message, '');

        }
         });
    
      }else{
        this.toastr.warning('Please fill out all fields correctly', '');

      } 
 

  }

  onUpdate(formValue) {

    if (this.Userform.valid) {

      console.log(formValue)
 
     
      this.ApiService.update_admin(formValue,this.taskid).subscribe(
        res => {
 
 
         let error: any = res["error"];
         let message: any = res["message"];
 
         if(error==4){
           this.toastr.warning(message, '');
 
         }else if(error==3){
           this.toastr.warning(message, '');
 
         }else if(error==0){
           this.toastr.success(' ', 'Update successfully');
           this.resetForm();
           this.ngOnInit();
           this.currentPage = "list";
         }else{
           this.toastr.warning(message, '');
 
         }
 
  
          });
     
       }else{
         this.toastr.warning('Please fill out all fields correctly', '');
 
         
         
       }
  


  }

  get formControls() {
    return this.Userform['controls'];
  }



  showPreview(event: any) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => this.imgSrc = e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.selectedImage = event.target.files[0];
    }
    else {
      this.imgSrc ="assets/img/image-placeholder.jpg" ;
      
      this.selectedImage = null;
    }
  }


  resetForm() {
    this.Userform.reset();
   
    this.imgSrc = "assets/img/image-placeholder.jpg";
    this.selectedImage = null;
    this.isSubmitted = false;
  }


  showPage(page: string) {
    this.currentPage = page;
    this.ngOnInit()
  }

  deleteTask(id) {

  
     swal.fire({
       title: 'Are you sure?',
       text: "You won't be able to revert this!",
       type: 'warning',
       showCancelButton: true,
       confirmButtonColor: '#3085d6',
       cancelButtonColor: '#d33',
       confirmButtonText: 'Yes, delete it!'
     }).then((result) => {
       if (result.value) {

        this.ApiService.delete_admin(id).subscribe(
          res => {
  
            this.ngOnInit();
          });
  

         swal.fire(
           'Deleted!',
           'Your use has been deleted.',
           'success'

           
         )
       }
     })
 
   
  }
  
  updatetask(page, id) {
    this.currentPage = page;
     this.taskid = id;

     this.ApiService.show_adminbyid(this.taskid).subscribe(
      res => {


        
        let resources: any[] = res["data"];
         this.usersarr=resources;

         this.Userform.setValue({
          first_name:this.usersarr['first_name'],
          last_name: this.usersarr['last_name'],
          email: this.usersarr['email'],
          phone: this.usersarr['phone'],
          passwords: this.usersarr['passwords'],
        // state_id: ""

        });

      
        
        // this.imgSrc=this.imageUrl+'/users/'+this.usersarr['image']


        
      });



  }
  
}
