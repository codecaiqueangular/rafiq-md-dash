import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../shared/services/api.service';
import swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';
import {Location} from '@angular/common';


@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {

  constructor(public toastr: ToastrService,
    private ApiService: ApiService,
     private cookieService: CookieService, private router: Router, 
     private route: ActivatedRoute , private _location :Location) {

   

      this.role = localStorage.getItem('rol');
      //console.log(this.role);
      this.show_blogcategory = localStorage.getItem('rol').includes("show_blogcategory");
      this.insert_blogcategory = localStorage.getItem('rol').includes("insert_blogcategory");
      this.update_blogcategory = localStorage.getItem('rol').includes("update_blogcategory");
      this.delete_blogcategory = localStorage.getItem('rol').includes("delete_blogcategory");
 /*  this.access = localStorage.getItem('rol').includes('اضافة صلاحيات'); */
     //  console.log( this.role)
     console.log(this.all)
     console.log(this.add)
    }
   all
   add
   delete
   update
   access
   show_blogcategory
   insert_blogcategory
   update_blogcategory
   delete_blogcategory

   role


  currentPage: string = "list";
  p: number = 1;
  taskid: string;
  imageUrl = environment.imageUrl;
  branchId
  imgSrc: string ="assets/img/image-placeholder.jpg";
  selectedImage: any = null;
  isSubmitted: boolean;
  usersarr:any[];
  show_Adminroles:any[];
  city:any[];
  
  Userform = new FormGroup({
    
    title: new FormControl('', Validators.required),
    E_title: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),  
    E_description: new FormControl('', Validators.required),  
    details: new FormControl('', Validators.required),
    E_details: new FormControl('', Validators.required),
    arrange: new FormControl('', Validators.required),
    category_id: new FormControl('',),

  })

  
  /*********** filter by name ***********************/
 
  AllUsers3
 
 
 

  filterbyName2=[]
  search2(filterBye){
    this.usersarr = this.AllUsers3
    this.filterbyName2 =[]
    for (let index = 0; index < this.usersarr.length; index++) {
       

      
      if((this.usersarr[index].title != null&& this.usersarr[index].title.includes(filterBye)) || 
      (this.usersarr[index].description != null && this.usersarr[index].description.toLowerCase().includes(filterBye.toLowerCase()))
      ||  
      (this.usersarr[index].arrange != null && this.usersarr[index].arrange.toLowerCase().includes(filterBye.toLowerCase()))
      || 
      (this.usersarr[index].details != null && this.usersarr[index].details.toLowerCase().includes(filterBye.toLowerCase()))
      ){
        this.filterbyName2.push(this.usersarr[index])
      }
      
    }
   this.usersarr = this.filterbyName2
  }


 

  /*********** filter by name ***********************/


  ngOnInit() {
    this.branchId=+this.route.snapshot.paramMap.get('id');

      
    this.ApiService.show_posts(this.branchId).subscribe(
      res => {

        let resources: any[] = res["data"];
        this.usersarr=resources;
        this.AllUsers3=resources;

      });

            
  }


  backfun(){
    this._location.back();
  }


   
  onSubmit(formValue) {
 
    if (this.Userform.valid) {    
      
      formValue["category_id"]=this.branchId;
     this.ApiService.add_post(formValue,this.selectedImage).subscribe(
       res => {
        let error: any = res["error"];
        let message: any = res["message"];
        if(error==4){
          this.toastr.warning(message, '');

        }else if(error==3){
          this.toastr.warning(message, '');

        }else if(error==0){
          this.toastr.success(' ', message);
          this.resetForm();
          this.ngOnInit();
          this.currentPage = "list";
        }else{
          this.toastr.warning(message, '');

        }
         });
    
      }else{
        this.toastr.warning('Please fill out all fields correctly', '');
      }
  }

  onUpdate(formValue) {

    if (this.Userform.valid) {
 
     
      this.ApiService.update_post(formValue,this.selectedImage,this.taskid).subscribe(
        res => {
 
 
         let error: any = res["error"];
         let message: any = res["message"];
 
         if(error==4){
           this.toastr.warning(message, '');
 
         }else if(error==3){
           this.toastr.warning(message, '');
 
         }else if(error==0){
           this.toastr.success(' ', 'Update successfully');
           this.resetForm();
           this.ngOnInit();
           this.currentPage = "list";
         }else{
           this.toastr.warning(message, '');
 
         }
   
          });
     
       }else{
         this.toastr.warning('Please fill out all fields correctly', '');
 
         
         
       }
  
  }

  get formControls() {
    return this.Userform['controls'];
  }



  showPreview(event: any) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => this.imgSrc = e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.selectedImage = event.target.files[0];
    }
    else {
      this.imgSrc ="assets/img/image-placeholder.jpg" ;      
      this.selectedImage = null;
    }
  }


  resetForm() {
    this.Userform.reset();
   
    this.imgSrc = "assets/img/image-placeholder.jpg";
    this.selectedImage = null;
    this.isSubmitted = false;
  }


  showPage(page: string) {
    this.currentPage = page;
    this.ngOnInit()
  }

  deleteTask(id) {

  
     swal.fire({
       title: 'Are you sure?',
       text: "You won't be able to revert this!",
       type: 'warning',
       showCancelButton: true,
       confirmButtonColor: '#3085d6',
       cancelButtonColor: '#d33',
       confirmButtonText: 'Yes, delete it!'
     }).then((result) => {
       if (result.value) {

        this.ApiService.delete_post(id).subscribe(
          res => {
  
            this.ngOnInit();
          });
  

         swal.fire(
           'Deleted!',
           'Your use has been deleted.',
           'success'

           
         )
       }
     })
 
   
  }
  
  updatetask(page, id) {
    this.currentPage = page;
     this.taskid = id;

     this.ApiService.show_postbyid(this.taskid).subscribe(
      res => {
        
        let resources: any[] = res["data"];
         this.usersarr=resources;

         this.Userform.setValue({
          title:this.usersarr['title'],
          E_title: this.usersarr['E_title'],
          description: this.usersarr['description'],
          E_description: this.usersarr['E_description'],
          details: this.usersarr['details'],
          E_details: this.usersarr['E_details'],
          arrange: this.usersarr['arrange'],
          category_id: this.branchId

        });

         this.imgSrc=this.imageUrl+'/posts/'+this.usersarr['image']


        
      });



  }
  
}
