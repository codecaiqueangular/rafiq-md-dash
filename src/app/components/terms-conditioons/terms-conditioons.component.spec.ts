import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermsConditioonsComponent } from './terms-conditioons.component';

describe('TermsConditioonsComponent', () => {
  let component: TermsConditioonsComponent;
  let fixture: ComponentFixture<TermsConditioonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermsConditioonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermsConditioonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
