import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../shared/services/api.service';
import swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-terms-conditioons',
  templateUrl: './terms-conditioons.component.html',
  styleUrls: ['./terms-conditioons.component.scss']
})
export class TermsConditioonsComponent implements OnInit {
  constructor(public toastr: ToastrService,
     private ApiService: ApiService, private cookieService: CookieService, private router: Router, private route: ActivatedRoute) {
  
  
      this.role = localStorage.getItem('rol');
      //console.log(this.role);
      this.all = localStorage.getItem('rol').includes("show_policy");
      this.update = localStorage.getItem('rol').includes("update_policy");
     /*  this.add = localStorage.getItem('rol').includes("insert_specialist");
      this.delete = localStorage.getItem('rol').includes("delete_specialist");
     
       this.access = localStorage.getItem('rol').includes('اضافة صلاحيات'); */
     //  console.log( this.role)
     console.log(this.all)
     console.log(this.add)
    }
   all
   add
   delete
   update
   access
   role
   
     currentPage: string = "list";
     p: number = 1;
     taskid: string;
     imgSrc: string ="assets/img/image-placeholder.jpg";
     selectedImage: any = null;
     isSubmitted: boolean;
     terms:any[];
     subjects:any[];
     subjectsarr:any[];
     content
     
     Userform = new FormGroup({
      text: new FormControl('', Validators.required),
      E_text: new FormControl('', Validators.required),
      
     })

     subject = new FormGroup({
      title: new FormControl('', Validators.required),
      E_title: new FormControl('', Validators.required),
      details: new FormControl('', Validators.required),
      E_details: new FormControl('', Validators.required),

     })
   
   
     ngOnInit() {

     this.ApiService.show_policy().subscribe(
       res => {
  
         let resources: any[] = res["data"];

         this.content = resources[0]["text"];


         let subjects: any[] = res["subjects"];
         this.subjects = subjects;

         
  
  
       });
   
      
       this.resetForm();
   
     }


     onSubmit(formValue) {
      if (this.subject.valid) {
 
        this.ApiService.insert_subject(formValue).subscribe(
          res => {
  
           this.toastr.success(' ', 'Done successfully');
           this.resetForm();
           this.ngOnInit();
           this.currentPage = "list";  
  
         });
      
        }else{
          this.toastr.warning('Please fill out all fields correctly', '');
  
        }
   
  

     }


     onUpdatexx(formValue) {
      if (this.subject.valid) {
 
        this.ApiService.update_subject(formValue,this.taskid).subscribe(
          res => {
  
           this.toastr.success(' ', 'Update successfully');
           this.resetForm();
           this.ngOnInit();
           this.currentPage = "list";  
  
         });
      
        }else{
          this.toastr.warning('Please fill out all fields correctly', '');
  
        }
   
  

     }
   
     onUpdate(formValue) {

      if (this.Userform.valid) {
 
        this.ApiService.update_policy(formValue).subscribe(
          res => {
  
           this.toastr.success(' ', 'Update successfully');
           this.resetForm();
           this.ngOnInit();
           this.currentPage = "list";  
  
         });
      
        }else{
          this.toastr.warning('Please fill out all fields correctly', '');
  
        }
   
  
   
     }

     deleteTask(id) {

  
      swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
 
      this.ApiService.delete_subject(id).subscribe(
        res => {
   
          this.ngOnInit();
        });
   
 
          swal.fire(
            'Deleted!',
            'Your use has been deleted.',
            'success'
 
            
          )
        }
      })
  
    
   }
 
   
   
     get formControls() {
       return this.Userform['controls'];
     }
   
   
     changepage(page) {
   
      this.currentPage = page;     

     
       
    }
    
     
     resetForm() {
       this.Userform.reset();
       this.subject.reset();

     }
     
   
     showPage(page: string) {
       this.currentPage = page;
       this.ngOnInit()
     }
   
  
     
     updatetask(page) {
   
       this.currentPage = page;     

        this.ApiService.show_policyByid().subscribe(
          res => {
    
            let resources: any[] = res["data"];
            this.terms = resources;

            this.Userform.setValue({
              text:  this.terms['text'],
              E_text: this.terms['E_text'],
      
              });
      
            
              
    
    
          });

     
   
        
     }
     
   
     updatetaskx(page,id) {
   
      this.currentPage = page;     
      this.taskid = id;     

       this.ApiService.show_subjectByid(id).subscribe(
         res => {
   
           let resources: any[] = res["data"];
           this.subjectsarr = resources;

           this.subject.setValue({
            title:  this.subjectsarr['title'],
            E_title: this.subjectsarr['E_title'],
            details: this.subjectsarr['details'],
            E_details: this.subjectsarr['E_details'],

             });
       
   
         });

    
  
       
    }
   

   
   }
   