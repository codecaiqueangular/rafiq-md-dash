import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpectialTypeComponent } from './spectial-type.component';

describe('SpectialTypeComponent', () => {
  let component: SpectialTypeComponent;
  let fixture: ComponentFixture<SpectialTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpectialTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpectialTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
