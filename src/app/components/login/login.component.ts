import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../shared/services/api.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  
  constructor(private cookieService: CookieService  ,public toastr: ToastrService,private ApiService:ApiService,private router:Router ,  ) { 


    

  }

  Userform = new FormGroup({
    email: new FormControl('', Validators.required),
    passwords: new FormControl('', Validators.required),
   
    })
  
  ngOnInit() {

    

  }
  rolesNames = ''
  onSubmit(formValue){
  
    
    if (this.Userform.valid) {

      
      this.ApiService.login(formValue).subscribe(
        res => {
            let error:number= res["error"];

            if(error==0){

              let result: any[] = res["data"];
              let first_name: any = result["first_name"];
              let user_token: any = result["user_token"];

              

              this.cookieService.set( 'first_name', first_name );
              this.cookieService.set( 'user_token', user_token );

              for(let i =0 ; i<res['roles'].length ; i++){
                this.rolesNames += res['roles'][i]['name'] + ","
              }
 

              this.cookieService.set( 'rol', this.rolesNames );
              localStorage.setItem('rol', this.rolesNames)

              this.toastr.success(' Welcome '+ first_name ,' ');
      

              this.router.navigate(['/dashboard']).then(() => {
               /*  window.location.reload(); */
                });

      
            }else{
              this.toastr.error(' ', 'المعلومات خطا ');
            }
      
          });


    
    
 
     }else{
      this.toastr.warning('Please fill out all fields correctly', '');

     }
    
     

    
  }

  
 // submitForm(email,password){


    // this.ApiService.admin_login(email.value,password.value).subscribe(
    //   data =>{
    //     let error:number= data["error"];

    //      if(error==0){
    //      this.router.navigate(['/dashboard'])
    //      }else{
    //       alert("error info")
           
    //      } 

    //     let res:any[]= data["data"];

    //     this.user_id= res["id"];
    //     this.user_name= res["first_name"];
    //     this.user_image= res["image"];
    //     this.user_email= res["email"];
    //     this.user_token= res["user_token"];

    //     this.cookieService.set('user_token', this.user_token );
    //     this.cookieService.set('user_id', this.user_id );
    //     this.cookieService.set('user_name', this.user_name );
    //     this.cookieService.set('user_image', this.user_image );
    //     this.cookieService.set('user_email', this.user_email );

    //   }
    // );

//}
  
}
