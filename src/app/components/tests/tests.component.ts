import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { finalize } from "rxjs/operators";
import { ApiService } from '../../shared/services/api.service';
import { map } from 'rxjs/operators';
import swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-tests',
  templateUrl: './tests.component.html',
  styleUrls: ['./tests.component.scss']
})
export class TestsComponent implements OnInit {


  constructor(public toastr: ToastrService, 
    private ApiService: ApiService, private cookieService: CookieService,
    private router: Router, private route: ActivatedRoute) {

      this.role = localStorage.getItem('rol');
      //console.log(this.role);
      this.all = localStorage.getItem('rol').includes("show_testes");
     /*  this.add = localStorage.getItem('rol').includes("insert_specialist");
      this.delete = localStorage.getItem('rol').includes("delete_specialist");
      this.update = localStorage.getItem('rol').includes("update_specialist");
       this.access = localStorage.getItem('rol').includes('اضافة صلاحيات'); */
     //  console.log( this.role)
     console.log(this.all)
     console.log(this.add)
    }
   all
   add
   delete
   update
   access
   role

  public loading = false;

  currentPage: string = "list";
  p: number = 1;
  taskid: string;
  imgSrc: string = "assets/img/image-placeholder.jpg";
  selectedImage: any = null;
  isSubmitted: boolean;
  usersarr: any[];
  branch: any[];
  branchD: any[];
  imageUrl = environment.imageUrl;

  lat: number = 30.044281;
  lng: number = 31.340002;
  locationChosen = false;

  
  Userform = new FormGroup({
    image: new FormControl('',),
    name: new FormControl('', Validators.required),
    E_name: new FormControl('', Validators.required),

  })


  ngOnInit() {

    this.ApiService.show_tests().subscribe(
      res => {

        let resources: any[] = res["data"];
        this.branch = resources;
        console.log(res)
      });

  }


  onSubmit(formValue) {

    if (this.Userform.valid) {

      this.ApiService.insert_test(formValue,this.selectedImage).subscribe(
        res => {
  
            this.toastr.success(' ', 'insert successfully');
            this.resetForm();
            this.ngOnInit();
            this.currentPage = "list";

          });
          
      
    } else {

      this.toastr.warning('Please fill out all fields correctly', '');

    }

  }

  onEdit(formValue) {

    if (this.Userform.valid) {

     this.ApiService.update_test(formValue,this.selectedImage,this.taskid).subscribe(
       res => {
  
       
           this.toastr.success(' ', 'Update  successfully');
           this.resetForm();
           this.ngOnInit();
           this.currentPage = "list";

         });
      
      
    } else {
      this.toastr.warning('Please fill out all fields correctly', '');

    }

  }


  showPreview(event: any) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => this.imgSrc = e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.selectedImage = event.target.files[0];
    }
    else {
      this.imgSrc ="assets/img/image-placeholder.jpg" ;
      
      this.selectedImage = null;
    }
  }
  

  get formControls() {
    return this.Userform['controls'];
  }

  
  resetForm() {
    this.Userform.reset();
   
  }


  showPage(page: string) {
    this.currentPage = page;
    this.resetForm()

    this.ngOnInit()
  }


  deleteTask(id) {

    swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {

       this.ApiService.delete_test(id).subscribe(
         res => {

           this.ngOnInit();
         });



        swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'


        )
      }
    })



  }


  updatetask(page, id) {


    this.currentPage = page;
    this.taskid = id;

     this.ApiService.show_onetest(id).subscribe(
       res => {

         let resources: any[] = res["data"];
         this.branchD = resources
         
         this.Userform.setValue({
          image: '',
          name: this.branchD[0]['name'],
          E_name: this.branchD[0]['E_name'],

         });

         this.imgSrc =this.imageUrl+ "/tests/" + this.branchD[0]['image'];



       });

  }



 
  

}
