import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { finalize } from "rxjs/operators";
import { map } from 'rxjs/operators';
import swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {

  constructor(public toastr: ToastrService, private cookieService: CookieService, private router: Router, private route: ActivatedRoute) {
   
  }


  currentPage: string = "list";
  p: number = 1;
  taskid: string;
  //users: Observable<any[]>;
  imgSrc: string ="assets/img/image-placeholder.jpg";
  selectedImage: any = null;
  isSubmitted: boolean;
  message:any[];

  
  Userform = new FormGroup({
   email: new FormControl('', Validators.required),
   phone: new FormControl('', Validators.required),
   message: new FormControl('', Validators.required),
   
  })


  ngOnInit() {

    // this.ImageServices.getcontactUs().snapshotChanges().pipe(
    //   map(changes =>
    //     changes.map(c =>
    //       ({ key: c.payload.key, ...c.payload.val() })

          
    //     )
    //   )
    // ).subscribe(branch => {
    //   this.message = branch;

      
    // });     



    this.resetForm();

  }


 // Success Type
 typeSuccess() {
   this.toastr.success('Update Successfuly !', 'Success!');
   }
 
  onSubmit(formValue) {
 

   if (this.Userform.valid) {

    //  this.ImageServices.insertcontactUs(formValue);
   
     this.typeSuccess();
      this.currentPage = "list";
      this.resetForm();

    }

  }


  get formControls() {
    return this.Userform['controls'];
  }

  
  resetForm() {
    this.Userform.reset();
    this.Userform.setValue({
      caption: '',
      imageUrl: '',
      category: 'Animal'
    });
    this.imgSrc = '/assets/img/image_placeholder.jpg';
    this.selectedImage = null;
    this.isSubmitted = false;
  }


  showPage(page: string) {
    this.currentPage = page;
    this.ngOnInit()
  }


  deleteTask(id) {

    swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {

        // this.ImageServices
        // .deletecontactUs(id)
        // .catch(err => console.log(err));

        swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'

          
        )
      }
    })
 
 
   
   }
 


}
