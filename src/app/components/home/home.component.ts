import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../shared/services/api.service';
import swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
 

  constructor(public toastr: ToastrService, 
    private ApiService: ApiService, private cookieService: CookieService,
    private router: Router, private route: ActivatedRoute) {


      this.all = localStorage.getItem('rol').includes("show_homedata");
      this.show_steps = localStorage.getItem('rol').includes("show_steps");
      this.update_steps = localStorage.getItem('rol').includes("update_steps");
      this.update = localStorage.getItem('rol').includes("update_homedata");
 /*  this.access = localStorage.getItem('rol').includes('اضافة صلاحيات'); */
     //  console.log( this.role)
     console.log(this.all)
     console.log(this.show_steps)
    }
   all
   show_steps
   update_steps
   update
   access
   role

  public loading = false;

  currentPage: string = "list";
  p: number = 1;
  taskid: string;
  imgSrc: string = "assets/img/image-placeholder.jpg";
  selectedImage: any = null;
  isSubmitted: boolean;
  usersarr: any[];
  branch: any[];
  branchD: any[];
  imageUrl = environment.imageUrl;

  Userform = new FormGroup({
    title: new FormControl('', Validators.required),
    E_title: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    E_description: new FormControl('', Validators.required),

  })



  
  form = new FormGroup({
    image: new FormControl('',),
    title: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    E_title: new FormControl('', Validators.required),
    E_description: new FormControl('', Validators.required),  
    video: new FormControl('', ),
   

  })


 // id 
  title
  E_title
  description
  E_description
step
  ngOnInit() {

    this.ApiService.show_homeData().subscribe(
      res => {

        let resources: any[] = res["data"];
        this.branch = resources;

        console.log(resources)

    //    this.id = res["data"].id
        this.title= res["data"].title
        this.E_title= res["data"].E_title
        this.description= res["data"].description
        this.E_description= res["data"].E_description
    
      });


      


      this.ApiService.show_steps().subscribe(
        res => {
  
          let resources: any[] = res["data"];
          this.step = resources;
  
          console.log(resources)


          this.ved = this.imageUrl + "/how_work/"+ this.step[0].video ;
  
   
      
        });


      this.resetForm();

  }


  ved

  onSubmit(formValue) {

    if (this.Userform.valid) {

      this.ApiService.update_homeData(formValue,this.selectedImage).subscribe(
        res => {
  
            this.toastr.success(' ', 'Update successfully');
            this.resetForm();
            this.ngOnInit();
            this.currentPage = "list";

          });
          
      
    } else {

      this.toastr.warning('Please fill out all fields correctly', '');

    }

  }

  onEdit(formValue) {

    if (this.Userform.valid) {

     this.ApiService.update_E3lan(formValue,this.selectedImage).subscribe(
       res => {
  
       
           this.toastr.success(' ', 'Update  successfully');
           this.resetForm();
           this.ngOnInit();
           this.currentPage = "list";

         });
      
      
    } else {
      this.toastr.warning('Please fill out all fields correctly', '');

    }

  }


  showPreview(event: any) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => this.imgSrc = e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.selectedImage = event.target.files[0];
    }
    else {
      this.imgSrc ="assets/img/image-placeholder.jpg" ;
      
      this.selectedImage = null;
    }
  }
  

  get formControls() {
    return this.Userform['controls'];
  }

  
  resetForm() {
    this.Userform.reset();
    this.form.reset();
    this.imgSrc = "assets/img/image-placeholder.jpg";
  }


  showPage(page: string) {
    this.currentPage = page;
    this.resetForm()

    this.ngOnInit()
  }


  deleteTask(id) {

    swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {

       this.ApiService.delete_specialist(id).subscribe(
         res => {

           this.ngOnInit();
         });



        swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'


        )
      }
    })



  }


  updatetask(page) {


    this.currentPage = page;
    





       this.Userform.setValue({
      //  image: '',
/*           name: this.branchD['name'],
        E_name: this.branchD['E_name'], */

        title:this.title,
     
        E_title:this.E_title,
        description:this.description,
        E_description:this.E_description,

       });

     //  this.imgSrc =this.imageUrl+ "/how_work//" + this.image;

  }
 


  
  updatetask2(page, id) {


    this.currentPage = page;
     this.taskid = id;

     this.ApiService.show_stepbyid(this.taskid).subscribe(
      res => {


        
        let resources: any[] = res["data"];
         this.usersarr=resources;


         console.log(this.usersarr)

          if(this.usersarr['video'] == null ||this.usersarr['video'] == undefined  ){
           document.getElementById('vedio').style.display = "none"
         }else if(this.usersarr['image'] == null ||this.usersarr['image'] == undefined  ){
          document.getElementById('image').style.display = "none"
         } 

         this.form.setValue({
          image:"",
          title: this.usersarr['title'],
          description: this.usersarr['description'],
          E_title: this.usersarr['E_title'],
          E_description: this.usersarr['E_description'],
          video: '',
         
       
      
          
      
        });

      
         if(this.usersarr['video'] == null ||this.usersarr['video'] == undefined  ){
          document.getElementById('vedio').style.display = "none"
          this.imgSrc=this.imageUrl +"/how_work/" +  this.usersarr['image']; 
        }else if(this.usersarr['image'] == null ||this.usersarr['image'] == undefined  ){
         document.getElementById('image').style.display = "none"
         this.imgSrc=  this.usersarr['video']; 
        }  

        
        


        
      });



  }
  

 
  
  
  onSubmit2(formValue) {
 
    if (this.form.valid) {
 
     
    /*   if(this.selectedImage == null ||this.selectedImage == undefined ){
        this.selectedImage  = this.imgSrc
      } */

      console.log(this.selectedImage)
      console.log(formValue)


      if( this.taskid == '2'  ){
          this.ApiService.update_steps(formValue,this.selectedImage , this.taskid).subscribe(
              res => {


                let error: any = res["error"];
                let message: any = res["message"];

                if(error==4){
                  this.toastr.warning(message, '');

                }else if(error==3){
                  this.toastr.warning(message, '');

                }else if(error==0){
                  this.toastr.success(' ', 'insert successfully');
                  this.resetForm();
                  this.ngOnInit();
                  this.currentPage = "step";
                }else{
                  this.toastr.warning(message, '');

                }

        
                });
      }else if ( this.taskid == '1' ){
        this.ApiService.update_steps_vedio(formValue,this.selectedImage , this.taskid).subscribe(
          res => {


            let error: any = res["error"];
            let message: any = res["message"];

            if(error==4){
              this.toastr.warning(message, '');

            }else if(error==3){
              this.toastr.warning(message, '');

            }else if(error==0){
              this.toastr.success(' ', 'insert successfully');
              this.resetForm();
              this.ngOnInit();
              this.currentPage = "step";
            }else{
              this.toastr.warning(message, '');

            }

    
            });
      }
     
  


         
    
      }else{
        this.toastr.warning('Please fill out all fields correctly', '');

      }
 

  }


  

}
