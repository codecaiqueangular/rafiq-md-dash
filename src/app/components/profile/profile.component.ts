import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { state } from '@angular/animations';
import { ApiService } from '../../shared/services/api.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  constructor(public toastr: ToastrService, private cookieService: CookieService,
    private router: Router, private ApiService: ApiService) {

     this.user_id = this.cookieService.get('user_id')

     }
 currentPage: string = "About"
 usersarr: any[];
 imageURL: string;
 user_id: any;
 p: number = 1;
 selectedImage:File=null;
 imgSrc: string ="assets/img/image-placeholder.jpg";
 imgSrc2: string ="assets/img/image-placeholder.jpg";

 isSubmitted: boolean;
 imageUrl = environment.imageUrl;

 first_name
 last_name
 phone
 email



Userform = new FormGroup({
 image: new FormControl('', ),
 first_name: new FormControl('', Validators.required),
 last_name: new FormControl('', Validators.required),
 phone: new FormControl('', Validators.required),
 email: new FormControl('', Validators.required),
 passwords: new FormControl('', Validators.required),
 
})



  

 handlefileInput(file:FileList){
   this.selectedImage =file.item(0);
   var reader=new FileReader();
   reader.onload=(event:any)=>{
     // this.ImageURl=event.target.result;
   }
   reader.readAsDataURL(this.selectedImage);
 }

 showPreview(event: any) {
   if (event.target.files && event.target.files[0]) {
     const reader = new FileReader();
     reader.onload = (e: any) => this.imgSrc = e.target.result;
     reader.readAsDataURL(event.target.files[0]);
     this.selectedImage = event.target.files[0];
   }
   else {
     this.imgSrc ="assets/img/image-placeholder.jpg" ;
     
     this.selectedImage = null;
   }
 }

get formControls() {
  return this.Userform['controls'];
}

 ngOnInit() {

   
   this.ApiService.show_adminById().subscribe(
     res => {


       
       let resources: any[] = res["data"];
        this.usersarr=resources;

        this.Userform.setValue({
         image:this.usersarr['image'],
         first_name: this.usersarr['first_name'],
         last_name: this.usersarr['last_name'],
         phone: this.usersarr['phone'],
         email: this.usersarr['email'],
         passwords: this.usersarr['passwords'],
       
         

       });

       this.first_name = this.usersarr['first_name'];
       this.last_name= this.usersarr['last_name']
       this.phone= this.usersarr['phone']
       this.email= this.usersarr['email']

       this.imgSrc=this.imageUrl+'/users/'+this.usersarr['image']


       
     });



   }

 showPage(page: string) {
   this.currentPage = page;
   this.ngOnInit()
 }


 onSubmit(formValue) {


  
   if (this.Userform.valid) {
    
     this.selectedImage
     
     this.ApiService.update_adminProfile(formValue, this.selectedImage).subscribe(
       res => {
 
         // let error: any[] = res["error"];
         // let message: any[] = res["message"];

         this.toastr.success(' ', ' Success');
         this.ngOnInit();
         this.currentPage = "About";

 
         });
       

    
     }else{
       this.toastr.warning(' برجاء مل جميع الحقول', '');

     }

 }

}
