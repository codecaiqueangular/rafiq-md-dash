import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../shared/services/api.service';
import swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
 @Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

 
  constructor(public toastr: ToastrService,
    private ApiService: ApiService,
     private cookieService: CookieService, private router: Router, private route: ActivatedRoute) {

   
      this.role =  localStorage.getItem('rol');
      //console.log(this.role);
      this.all =  localStorage.getItem('rol').includes("show_Notifications");
    this.Student =  localStorage.getItem('rol').includes("Send_Notification_To_Student");
    this.Trainer =  localStorage.getItem('rol').includes("Send_Notification_To_Trainer");
    this.WlyAlamr =  localStorage.getItem('rol').includes("Send_Notification_To_WlyAlamr");
    this.Special =  localStorage.getItem('rol').includes("Send_Notification_To_Special");
 
 /*      this.update =  localStorage.getItem('rol').includes("تعديل موظف ");
       this.access =  localStorage.getItem('rol').includes('اضافة صلاحيات'); */
     //  console.log( this.role)

    }
   all
   Trainer
   Student
   WlyAlamr
   Special
 
   
   update
   access
   role


  currentPage: string = "list";
  p: number = 1;
  taskid: string;
  imageUrl = environment.imageUrl;

  imgSrc: string ="assets/img/image-placeholder.jpg";
  selectedImage: any = null;
  isSubmitted: boolean;
  show_students:any[];
  show_trainer:any[];
  show_wlyAlamer:any[];
  show_blockedstudent:any[];
  show_blockedwlyamer:any[];
  AllUsers:any[];

  Userform = new FormGroup({
    cash: new FormControl('', Validators.required),
    user_id: new FormControl('',),
  })

  Userform1 = new FormGroup({
    
    body: new FormControl('',),
    title: new FormControl('', ),
    title_ar: new FormControl('',),
    body_ar: new FormControl('', ),
   
  })

  Userform2 = new FormGroup({
    
    body: new FormControl('',),
    title: new FormControl('', ),
    title_ar: new FormControl('',),
    body_ar: new FormControl('', ),
   
  })
  Userform3 = new FormGroup({
    
    body: new FormControl('',),
    title: new FormControl('', ),
    title_ar: new FormControl('',),
    body_ar: new FormControl('', ),
   
  })
  Userform4 = new FormGroup({
    
    body: new FormControl('',),
    title: new FormControl('', ),
    title_ar: new FormControl('',),
    body_ar: new FormControl('', ),
   
  })
  show_pending_trainer



  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};



  ngOnInit() {



    
    this.selectedItems = [
     
    ];
    this.dropdownSettings  = {
      singleSelection: true,
      itemsShowLimit:15,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      
      
      allowSearchFilter: true
    };




        
 
     this.ApiService.show_all_usersss().subscribe(
       res => {
         let resources: any[] = res["data"];
        
         
         this.AllUsers=resources;
      
         for(let i = 0 ; i<this.AllUsers.length ; i++){

          if(this.AllUsers[i].full_name  && this.AllUsers[i].id ){
    
            this.dropdownList[i]={item_id:this.AllUsers[i].id , item_text:`${this.AllUsers[i].full_name
           }`
          
          }
          }
                
        } 
     
       });
      
     
    
    this.resetForm();

  }

  
  
   


  send_studentNotification(formValue){


    if (this.Userform1.valid) {


      
      this.ApiService.send_studentNotification(formValue).subscribe(
        res => {
  
          let message: any = res["message"];

            this.toastr.success(' ', message);
            this.resetForm();
            this.ngOnInit();
           

          });
       
      
    } else {
      this.toastr.warning('Please fill out all fields correctly', '');

    }



    
  }



  send_trainerNotification(formValue){


    if (this.Userform2.valid) {


      
      this.ApiService.send_trainerNotification(formValue).subscribe(
        res => {
  
          let message: any = res["message"];

            this.toastr.success(' ', message);
            this.resetForm();
            this.ngOnInit();
           

          });
       
      
    } else {
      this.toastr.warning('Please fill out all fields correctly', '');

    }



    
  }


  send_parentNotification(formValue){


    if (this.Userform3.valid) {


      
      this.ApiService.send_parentNotification(formValue).subscribe(
        res => {
  
          let message: any = res["message"];

            this.toastr.success(' ', message);
            this.resetForm();
            this.ngOnInit();
           

          });
       
      
    } else {
      this.toastr.warning('Please fill out all fields correctly', '');

    }



    
  }

  ssss=""
  send_specialNotification(formValue){


    if (this.Userform4.valid) {

    

        console.log(this.selectedItems[0].item_id)

      
      this.ApiService.send_specialNotification(formValue ,this.selectedItems[0].item_id).subscribe(
        res => {
  
          let message: any = res["message"];

            this.toastr.success(' ', message);
            this.resetForm();
            this.ngOnInit();
           

          });
       
      
    } else {
      this.toastr.warning('Please fill out all fields correctly', '');

    }



    
  }

  onSubmit(formValue) {
    if (this.Userform.valid) {


      formValue["user_id"]=this.taskid;
      this.ApiService.recharge_wallet(formValue).subscribe(
        res => {
  
          let message: any = res["message"];

            this.toastr.success(' ', message);
            this.resetForm();
            this.ngOnInit();
            this.currentPage = "list";

          });
       
      
    } else {
      this.toastr.warning('Please fill out all fields correctly', '');

    }


  }


  wallet(page,id) {
    this.currentPage = page;
    this.taskid = id;

   }
 


  get formControls() {
    return this.Userform['controls'];
  }



  showPreview(event: any) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => this.imgSrc = e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.selectedImage = event.target.files[0];
    }
    else {
      this.imgSrc ="assets/img/image-placeholder.jpg" ;
      
      this.selectedImage = null;
    }
  }


  resetForm() {
    this.Userform1.reset();
    this.Userform2.reset();
    this.Userform3.reset();
    this.Userform4.reset();
   
  }

  


  showPage(page: string) {
    this.currentPage = page;
    this.ngOnInit()
  }

  deleteTask(id) {

  
     swal.fire({
       title: 'Are you sure?',
       type: 'warning',
       showCancelButton: true,
       confirmButtonColor: '#3085d6',
       cancelButtonColor: '#d33',
       confirmButtonText: 'Yes, block it!'
     }).then((result) => {
       if (result.value) {

       this.ApiService.block_user(id).subscribe(
         res => {
  
           this.ngOnInit();
         });
  

         swal.fire(
           'block!',
           'Your use has been block.',
           'success'

           
         )
       }
     })
 
   
  }


  unblock_user(id) {

  
    swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Unblock it!'
    }).then((result) => {
      if (result.value) {

      this.ApiService.unblock_user(id).subscribe(
        res => {
 
          this.ngOnInit();
        });
 

        swal.fire(
          'Un Blocked!',
          'Your use has been UnBlocked.',
          'success'

          
        )
      }
    })

  
 }



 unblock_user2(id) {

  
  swal.fire({
    title: 'Are you sure?',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, Accept him!'
  }).then((result) => {
    if (result.value) {

    this.ApiService.unblock_user(id).subscribe(
      res => {

        this.ngOnInit();
      });


      swal.fire(
        ' Accepted!',
        'Your use has been Accepted.',
        'success'

        
      )
    }
  })


}


}
