import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../shared/services/api.service';
import swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-specialists',
  templateUrl: './specialists.component.html',
  styleUrls: ['./specialists.component.scss']
})
export class SpecialistsComponent implements OnInit {


  constructor(public toastr: ToastrService, 
    private ApiService: ApiService, private cookieService: CookieService,
    private router: Router, private route: ActivatedRoute) {


      this.role = localStorage.getItem('rol');
      //console.log(this.role);
      this.all = localStorage.getItem('rol').includes("show_specialists");
      this.add = localStorage.getItem('rol').includes("insert_specialist");
      this.delete = localStorage.getItem('rol').includes("delete_specialist");
      this.update = localStorage.getItem('rol').includes("update_specialist");
      /*  this.access = localStorage.getItem('rol').includes('اضافة صلاحيات'); */
     //  console.log( this.role)
     console.log(this.all)
     console.log(this.add)
    }
   all
   add
   delete
   update
   access
   role

  public loading = false;

  currentPage: string = "list";
  p: number = 1;
  taskid: string;
  imgSrc: string = "assets/img/image-placeholder.jpg";
  selectedImage: any = null;
  isSubmitted: boolean;
  usersarr: any[];
  branch: any[];
  branchD: any[];
  imageUrl = environment.imageUrl;

  Userform = new FormGroup({
    image: new FormControl('',),
    name: new FormControl('', Validators.required),
    E_name: new FormControl('', Validators.required),

  })
  filterbyName3=[]
  AllUsers4
  search3(filterBye){
    this.branch = this.AllUsers4
    this.filterbyName3 =[]
    for (let index = 0; index < this.branch.length; index++) {
      if(this.branch[index].name !=null )

      
      if(this.branch[index].name.toLowerCase().includes(filterBye.toLowerCase())){
        this.filterbyName3.push(this.branch[index])
      }
      
    }
   this.branch = this.filterbyName3
  }

  ngOnInit() {

    this.ApiService.show_specialists().subscribe(
      res => {

        let resources: any[] = res["data"];
        this.branch = resources;
        this.AllUsers4=resources;
      });
      this.resetForm();

  }


  onSubmit(formValue) {

    if (this.Userform.valid) {

      this.ApiService.insert_specialists(formValue,this.selectedImage).subscribe(
        res => {
  
            this.toastr.success(' ', 'insert successfully');
            this.resetForm();
            this.ngOnInit();
            this.currentPage = "list";

          });
          
      
    } else {

      this.toastr.warning('Please fill out all fields correctly', '');

    }

  }

  onEdit(formValue) {

    if (this.Userform.valid) {

     this.ApiService.update_specialists(formValue,this.selectedImage,this.taskid).subscribe(
       res => {
  
       
           this.toastr.success(' ', 'Update  successfully');
           this.resetForm();
           this.ngOnInit();
           this.currentPage = "list";

         });
      
      
    } else {
      this.toastr.warning('Please fill out all fields correctly', '');

    }

  }


  showPreview(event: any) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => this.imgSrc = e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.selectedImage = event.target.files[0];
    }
    else {
      this.imgSrc ="assets/img/image-placeholder.jpg" ;
      
      this.selectedImage = null;
    }
  }
  

  get formControls() {
    return this.Userform['controls'];
  }

  
  resetForm() {
    this.Userform.reset();
    this.imgSrc = "assets/img/image-placeholder.jpg";
  }


  showPage(page: string) {
    this.currentPage = page;
    this.resetForm()

    this.ngOnInit()
  }


  deleteTask(id) {

    swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {

       this.ApiService.delete_specialist(id).subscribe(
         res => {

           this.ngOnInit();
         });



        swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'


        )
      }
    })



  }


  updatetask(page, id) {


    this.currentPage = page;
    this.taskid = id;

     this.ApiService.show_specialistsByid(id).subscribe(
       res => {

         let resources: any[] = res["data"];
         this.branchD = resources

      
         
         this.Userform.setValue({
          image: '',
          name: this.branchD['name'],
          E_name: this.branchD['E_name'],

         });

         this.imgSrc =this.imageUrl+ "/specialist/" + this.branchD['image'];



       });

  }



 
  

}
