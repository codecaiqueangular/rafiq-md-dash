import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../shared/services/api.service';
import swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-institutions',
  templateUrl: './institutions.component.html',
  styleUrls: ['./institutions.component.scss']
})
export class InstitutionsComponent implements OnInit {

  
 
  constructor(public toastr: ToastrService,
    private ApiService: ApiService,
     private cookieService: CookieService, private router: Router, private route: ActivatedRoute) {

   
      this.role = localStorage.getItem('rol');
      //console.log(this.role);
      this.all = localStorage.getItem('rol').includes("show_institutions");
      this.add = localStorage.getItem('rol').includes("insert_institutions");
      this.delete = localStorage.getItem('rol').includes("insert_institutions");
      this.update = localStorage.getItem('rol').includes("update_institutions");
 /*  this.access = localStorage.getItem('rol').includes('اضافة صلاحيات'); */
     //  console.log( this.role)
     console.log(this.all)
     console.log(this.add)
    }
   all
   add
   delete
   update
   access

   role


  currentPage: string = "list";
  p: number = 1;
  taskid: string;
  imageUrl = environment.imageUrl;

  imgSrc: string ="assets/img/image-placeholder.jpg";
  selectedImage: any = null;
  isSubmitted: boolean;
  usersarr:any[];
  city:any[];
  
  Userform = new FormGroup({
    image: new FormControl('',),
    name: new FormControl('', Validators.required),
    E_name: new FormControl('', Validators.required),
    phone: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),  
    passwords: new FormControl('', Validators.required),
    class_number: new FormControl('', Validators.required),

  })




  
  /*********** filter by name ***********************/
  AllUsers2
  AllUsers4
  AllUsers3
  _listFilter = '';
  filteredAllDiscussion: any[] = [];
  get listFilter(): string {
    return this._listFilter;
  }
 
 

  filterbyName2=[]
  search2(filterBye){
    this.usersarr = this.AllUsers3
    this.filterbyName2 =[]
    for (let index = 0; index < this.usersarr.length; index++) {
     

      
      if(this.usersarr[index].name != null && this.usersarr[index].name.toLowerCase().includes(filterBye.toLowerCase())
      || 
      this.usersarr[index].phone != null && this.usersarr[index].phone.toLowerCase().includes(filterBye.toLowerCase())
      || 
      this.usersarr[index].email != null && this.usersarr[index].email.toLowerCase().includes(filterBye.toLowerCase())
      || 
      this.usersarr[index].class_number != null && this.usersarr[index].class_number.toLowerCase().includes(filterBye.toLowerCase())
      ){
        this.filterbyName2.push(this.usersarr[index])
      }
      
    }
   this.usersarr = this.filterbyName2
  }


 

  /*********** filter by name ***********************/




  ngOnInit() {

      
    this.ApiService.show_institutions().subscribe(
      res => {

        let resources: any[] = res["data"];
        this.usersarr=resources;
        this.AllUsers3=resources;

        
      });

      
      
  }

   
  onSubmit(formValue) {
 
    if (this.Userform.valid) {
 
     
     this.ApiService.insert_institutions(formValue,this.selectedImage).subscribe(
       res => {


        let error: any = res["error"];
        let message: any = res["message"];

        if(error==4){
          this.toastr.warning(message, '');

        }else if(error==3){
          this.toastr.warning(message, '');

        }else if(error==0){
          this.toastr.success(' ', 'insert successfully');
          this.resetForm();
          this.ngOnInit();
          this.currentPage = "list";
        }else{
          this.toastr.warning(message, '');

        }

 
         });
    
      }else{
        this.toastr.warning('Please fill out all fields correctly', '');

      }
 

  }




  onUpdate(formValue) {

    if (this.Userform.valid) {
 
     
      this.ApiService.update_institution(formValue,this.selectedImage,this.taskid).subscribe(
        res => {
 
 
         let error: any = res["error"];
         let message: any = res["message"];
 
         if(error==4){
           this.toastr.warning(message, '');
 
         }else if(error==3){
           this.toastr.warning(message, '');
 
         }else if(error==0){
           this.toastr.success(' ', 'insert successfully');
           this.resetForm();
           this.ngOnInit();
           this.currentPage = "list";
         }else{
           this.toastr.warning(message, '');
 
         }
 
  
          });
     
       }else{
         this.toastr.warning('Please fill out all fields correctly', '');
 
         
         
       }
  


  }

  get formControls() {
    return this.Userform['controls'];
  }



  showPreview(event: any) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => this.imgSrc = e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.selectedImage = event.target.files[0];
    }
    else {
      this.imgSrc ="assets/img/image-placeholder.jpg" ;
      
      this.selectedImage = null;
    }
  }


  resetForm() {
    this.Userform.reset();
   
    this.imgSrc = "assets/img/image-placeholder.jpg";
    this.selectedImage = null;
    this.isSubmitted = false;
  }


  showPage(page: string) {
    this.currentPage = page;
    this.ngOnInit()
  }

  deleteTask(id) {

  
     swal.fire({
       title: 'Are you sure?',
       text: "You won't be able to revert this!",
       type: 'warning',
       showCancelButton: true,
       confirmButtonColor: '#3085d6',
       cancelButtonColor: '#d33',
       confirmButtonText: 'Yes, delete it!'
     }).then((result) => {
       if (result.value) {

        this.ApiService.delete_institutions(id).subscribe(
          res => {
  
            this.ngOnInit();
          });
  

         swal.fire(
           'Deleted!',
           'Your use has been deleted.',
           'success'

           
         )
       }
     })
 
   
  }


  
  updatetask(page, id) {


    this.currentPage = page;
     this.taskid = id;

     this.ApiService.show_institutionByid(this.taskid).subscribe(
      res => {


        
        let resources: any[] = res["data"];
         this.usersarr=resources;


         console.log(this.usersarr)

         this.Userform.setValue({
          image:"",
          name: this.usersarr['name'],
          E_name: this.usersarr['E_name'],
          phone: this.usersarr['phone'],
          email: this.usersarr['email'],
          passwords: this.usersarr['passwords'],
          class_number: this.usersarr['class_number'],
       
      
          
      
        });

      
        
        this.imgSrc=""


        
      });



  }
  

 

  
  

}
