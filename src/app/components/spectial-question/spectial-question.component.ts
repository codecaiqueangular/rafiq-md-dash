import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../shared/services/api.service';
import swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';
import {Location} from '@angular/common';


@Component({
  selector: 'app-spectial-question',
  templateUrl: './spectial-question.component.html',
  styleUrls: ['./spectial-question.component.scss']
})
export class SpectialQuestionComponent implements OnInit {

  constructor(public toastr: ToastrService,
    private ApiService: ApiService,
     private cookieService: CookieService, private router: Router, private route: ActivatedRoute , private _location :Location) {
   
      this.role = localStorage.getItem('rol');
      this.show_specialquestions =  localStorage.getItem('rol').includes("show_specialquestions");
      this.insert_specialquestion =  localStorage.getItem('rol').includes("insert_specialquestion");
      this.update_special_questions = localStorage.getItem('rol').includes("update_special_questions");
      this.delete = localStorage.getItem('rol').includes("delete_type");
   
  }


  show_specialquestions
  insert_specialquestion
  update_special_questions
  role
  delete


  currentPage: string = "list";
  p: number = 1;
  taskid: string;
  imageUrl = environment.imageUrl;

  imgSrc: string ="assets/img/image-placeholder.jpg";
  selectedImage: any = null;
  isSubmitted: boolean;
  branch:any[];
  branchD:any[];
  branchId:any;
  
  Userform = new FormGroup({
    question: new FormControl('', Validators.required),
    E_question: new FormControl('', Validators.required),
    answer: new FormControl('', Validators.required),
    E_answer: new FormControl('', Validators.required),
    type_id: new FormControl('', ),

  })
  filterbyName3=[]
  AllUsers4
  search3(filterBye){
    this.branch = this.AllUsers4
    this.filterbyName3 =[]
    for (let index = 0; index < this.branch.length; index++) {
      if(this.branch[index].question !=null )

      
      if(this.branch[index].question !=null &&this.branch[index].question.toLowerCase().includes(filterBye.toLowerCase())
      ||
      this.branch[index].answer !=null &&this.branch[index].answer.toLowerCase().includes(filterBye.toLowerCase())
      ){
        this.filterbyName3.push(this.branch[index])
      }
      
    }
   this.branch = this.filterbyName3
  }

  ngOnInit() {
    this.branchId=+this.route.snapshot.paramMap.get('id');

     this.ApiService.show_typeByid(this.branchId).subscribe(
       res => {

         let resources: any[] = res["questions"];
         this.branch=resources;
         this.AllUsers4=resources;

       });

  }

  

   
  onSubmit(formValue) {
 
    
    if (this.Userform.valid) {
 

      formValue["type_id"]= this.branchId ;
     
      this.ApiService.insert_question(formValue).subscribe(
        res => {

         this.toastr.success(' ', 'insert successfully');
         this.resetForm();
         this.ngOnInit();
         this.currentPage = "list";  

       });
    
      }else{
        this.toastr.warning('Please fill out all fields correctly', '');

      }
 

  }


  onUpdate(formValue) {

    
    if (this.Userform.valid) {
 

      formValue["type_id"]= this.branchId ;
     
      this.ApiService.update_question(formValue,this.taskid).subscribe(
        res => {

         this.toastr.success(' ', 'Update successfully');
         this.resetForm();
         this.ngOnInit();
         this.currentPage = "list";  

       });
    
      }else{
        this.toastr.warning('Please fill out all fields correctly', '');

      }
 



  }

  get formControls() {
    return this.Userform['controls'];
  }


  resetForm() {
    this.Userform.reset();
   
    this.imgSrc = "assets/img/image-placeholder.jpg";
    this.selectedImage = null;
    this.isSubmitted = false;
  }


  showPage(page: string) {
    this.currentPage = page;
    this.ngOnInit()
  }

  deleteTask(id) {

  
     swal.fire({
       title: 'Are you sure?',
       text: "You won't be able to revert this!",
       type: 'warning',
       showCancelButton: true,
       confirmButtonColor: '#3085d6',
       cancelButtonColor: '#d33',
       confirmButtonText: 'Yes, delete it!'
     }).then((result) => {
       if (result.value) {

     this.ApiService.delete_question(id).subscribe(
       res => {
  
         this.ngOnInit();
       });
  

         swal.fire(
           'Deleted!',
           'Your use has been deleted.',
           'success'

           
         )
       }
     })
 
   
  }


  updatetask(page, id) {


    this.currentPage = page;
     this.taskid = id;


     this.ApiService.show_questionByid(id).subscribe(
      res => {
      
        let resources: any[] = res["data"];
        this.branchD = resources

        this.Userform.setValue({
          type_id: this.branchId,
          question: this.branchD['question'],
          E_question: this.branchD['E_question'],
          answer: this.branchD['answer'],
          E_answer: this.branchD['E_answer'],

        });
      
        
      });


  }
  


  backfun(){

    this._location.back();
  }

  
  

}
