import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpectialQuestionComponent } from './spectial-question.component';

describe('SpectialQuestionComponent', () => {
  let component: SpectialQuestionComponent;
  let fixture: ComponentFixture<SpectialQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpectialQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpectialQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
