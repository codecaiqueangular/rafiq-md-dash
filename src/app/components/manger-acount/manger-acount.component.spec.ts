import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MangerAcountComponent } from './manger-acount.component';

describe('MangerAcountComponent', () => {
  let component: MangerAcountComponent;
  let fixture: ComponentFixture<MangerAcountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MangerAcountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MangerAcountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
