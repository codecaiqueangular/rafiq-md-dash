import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { finalize } from "rxjs/operators";
import { ApiService } from '../../shared/services/api.service';
import { map } from 'rxjs/operators';
import swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-manger-acount',
  templateUrl: './manger-acount.component.html',
  styleUrls: ['./manger-acount.component.scss']
})
export class MangerAcountComponent implements OnInit {

  
  
 
  constructor(public toastr: ToastrService,
    private ApiService: ApiService,
     private cookieService: CookieService, private router: Router, private route: ActivatedRoute) {

   
  }


  currentPage: string = "list";
  p: number = 1;
  taskid: string;
  imageUrl = environment.imageUrl;

  imgSrc: string ="assets/img/image-placeholder.jpg";
  selectedImage: any = null;
  isSubmitted: boolean;
  usersarr:any[];
  city:any[];
  
  Userform = new FormGroup({
    image: new FormControl('',),
    first_name: new FormControl('', Validators.required),
    last_name: new FormControl('', Validators.required),
    phone: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    Latitude: new FormControl('', ),
    Longitude: new FormControl('', ),

  })


  ngOnInit() {

      
    // this.ApiService.show_all_bonbeAdmins().subscribe(
    //   res => {

    //     let resources: any[] = res["data"];
    //     this.usersarr=resources;
        

        
    //   });

      
      
  }

  

   
  onSubmit(formValue) {
 
    
    if (this.Userform.valid) {
 
     
    //  this.ApiService.add_bonbel_admin(formValue,this.selectedImage).subscribe(
    //    res => {


    //     let error: any = res["error"];
    //     let message: any = res["message"];

    //     if(error==4){
    //       this.toastr.warning(message, '');

    //     }else if(error==3){
    //       this.toastr.warning(message, '');

    //     }else if(error==0){
    //       this.toastr.success(' ', 'insert successfully');
    //       this.resetForm();
    //       this.ngOnInit();
    //       this.currentPage = "list";
    //     }else{
    //       this.toastr.warning(message, '');

    //     }

 
    //      });
    
      }else{
        this.toastr.warning('Please fill out all fields correctly', '');

        
        
      }
 

  }


  get formControls() {
    return this.Userform['controls'];
  }



  showPreview(event: any) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => this.imgSrc = e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.selectedImage = event.target.files[0];
    }
    else {
      this.imgSrc ="assets/img/image-placeholder.jpg" ;
      
      this.selectedImage = null;
    }
  }


  resetForm() {
    this.Userform.reset();
   
    this.imgSrc = "assets/img/image-placeholder.jpg";
    this.selectedImage = null;
    this.isSubmitted = false;
  }


  showPage(page: string) {
    this.currentPage = page;
    this.ngOnInit()
  }

  deleteTask(id) {

  
     swal.fire({
       title: 'Are you sure?',
       text: "You won't be able to revert this!",
       type: 'warning',
       showCancelButton: true,
       confirmButtonColor: '#3085d6',
       cancelButtonColor: '#d33',
       confirmButtonText: 'Yes, delete it!'
     }).then((result) => {
       if (result.value) {

        // this.ApiService.delete_user(id).subscribe(
        //   res => {
  
        //     this.ngOnInit();
        //   });
  

         swal.fire(
           'Deleted!',
           'Your use has been deleted.',
           'success'

           
         )
       }
     })
 
   
  }

  banTask(id) {

    
    swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {

      //  this.ApiService.delete_user(id).subscribe(
      //    res => {
 
      //      this.ngOnInit();
      //    });
 

        swal.fire(
          'Deleted!',
          'Your use has been deleted.',
          'success'

          
        )
      }
    })

  
 }

  updatetask(page, id) {


    this.currentPage = page;
     this.taskid = id;

  }
  


  onUpdate(formValue) {

    if (this.Userform.valid) {
 
     
      this.ApiService.update_adminProfile(formValue,this.selectedImage).subscribe(
        res => {
 
 
         let error: any = res["error"];
         let message: any = res["message"];
 
         if(error==4){
           this.toastr.warning(message, '');
 
         }else if(error==3){
           this.toastr.warning(message, '');
 
         }else if(error==0){
           this.toastr.success(' ', 'insert successfully');
           this.resetForm();
           this.ngOnInit();
           this.currentPage = "list";
         }else{
           this.toastr.warning(message, '');
 
         }
 
  
          });
     
       }else{
         this.toastr.warning('Please fill out all fields correctly', '');
 
         
         
       }
  


  }


  
  

}
