import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviousDealingsComponent } from './previous-dealings.component';

describe('PreviousDealingsComponent', () => {
  let component: PreviousDealingsComponent;
  let fixture: ComponentFixture<PreviousDealingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviousDealingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviousDealingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
