import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../shared/services/api.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})

export class AboutUsComponent implements OnInit {

  title
  E_title
  address
  E_address
  phone
  email
  details
  E_details
  constructor(public toastr: ToastrService,
    private ApiService: ApiService, private cookieService: CookieService,
    private router: Router, private route: ActivatedRoute) {

   

   
      this.role = localStorage.getItem('rol');
      //console.log(this.role);
      this.all = localStorage.getItem('rol').includes("about_us");
      this.add = localStorage.getItem('rol').includes("insert_questiontype");
      this.delete = localStorage.getItem('rol').includes("delete_type");
      this.update = localStorage.getItem('rol').includes("update_questiontypes");
 /*  this.access = localStorage.getItem('rol').includes('اضافة صلاحيات'); */
     //  console.log( this.role)
     console.log(this.all)
     console.log(this.add)
    }
   all
   add
   delete
   update
   access

   role



  currentPage: string = "list";
  p: number = 1;
  taskid: string;
  face_link
  insta_link
  linked_link
  twitter_link
  imgSrc: string = "assets/img/image-placeholder.jpg";
  selectedImage: any = null;
  isSubmitted: boolean;
  terms: any[];


  Userform = new FormGroup({
    image: new FormControl(''),
    title: new FormControl('', Validators.required),
    E_title: new FormControl('', Validators.required),
    address: new FormControl('', Validators.required),
    E_address: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    phone: new FormControl('', Validators.required),
    details: new FormControl('', Validators.required),
    e_details: new FormControl('', Validators.required),
    face_link: new FormControl('',),
    insta_link: new FormControl('',),
    linked_link: new FormControl('', ),
    twitter_link: new FormControl('',),
  })



  ngOnInit() {

    this.ApiService.show_about().subscribe(
      res => {

        let resources: any[] = res["about_data"];
        this.title = resources["title"];
        this.E_title = resources["E_title"];
        this.address = resources["address"];
        this.E_address = resources["E_address"];
        this.phone = resources["phone"];
        this.email = resources["email"];
        this.details = resources["details"];
        this.E_details = resources["E_details"];
        this.face_link = resources["face_link"];
        this.insta_link = resources["insta_link"];
        this.linked_link = resources["linked_link"];
        this.twitter_link = resources["twitter_link"];

      });


    this.resetForm();

  }

  showPreview(event: any) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => this.imgSrc = e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.selectedImage = event.target.files[0];
    }
    else {
      this.imgSrc ="assets/img/image-placeholder.jpg" ;
      
      this.selectedImage = null;
    }
  }

  get formControls() {
    return this.Userform['controls'];
  }

  resetForm() {
    this.Userform.reset();
    this.imgSrc ="assets/img/image-placeholder.jpg" ;

  }


  showPage(page: string) {
    this.currentPage = page;
    this.ngOnInit()
  }



  updatetask(page) {


    this.currentPage = page;
    


    this.Userform.setValue({
      image: '',
      title:  this.title ,
      E_title:  this.E_title ,
      address:  this.address ,
      E_address:  this.E_address ,
      phone: this.phone ,
      email: this.email ,
      details: this.details ,
      e_details:  this.E_details ,
      face_link:  this.face_link,
      insta_link:  this.insta_link,
      linked_link:  this.linked_link,
      twitter_link:  this.twitter_link,

      
    });




  }



  onUpdate(formValue) {


    if (this.Userform.valid) {

      this.ApiService.update_about(formValue,this.selectedImage).subscribe(
        res => {

         this.toastr.success(' ', 'Update successfully');
         this.ngOnInit();
          this.resetForm();
          this.currentPage = "list";

        });



    } else {
      this.toastr.warning('Please fill out all fields correctly', '');

    }


  }



}
