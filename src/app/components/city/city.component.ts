import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../shared/services/api.service';
import swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';
import {Location} from '@angular/common';


@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.scss']
})
export class CityComponent implements OnInit {


  constructor(public toastr: ToastrService,
    private ApiService: ApiService,
     private cookieService: CookieService, private router: Router,
      private route: ActivatedRoute , private _location :Location) {

        this.role = localStorage.getItem('rol');
        //console.log(this.role);
        this.all = localStorage.getItem('rol').includes("show_country");
        this.add = localStorage.getItem('rol').includes("insert_country");
        this.delete = localStorage.getItem('rol').includes("delete_country");
        this.update = localStorage.getItem('rol').includes("update_country");
   /*  this.access = localStorage.getItem('rol').includes('اضافة صلاحيات'); */
       //  console.log( this.role)
       console.log(this.all)
       console.log(this.add)
      }
     all
     add
     delete
     update
     access
  
     role
  

  currentPage: string = "list";
  p: number = 1;
  taskid: string;
  imageUrl = environment.imageUrl;
  imgSrc: string ="assets/img/image-placeholder.jpg";
  selectedImage: any = null;
  isSubmitted: boolean;
  branch:any[];
  branchD:any[];
  branchId:any;
  
  Userform = new FormGroup({
    a_name: new FormControl('', Validators.required),
    E_name: new FormControl('', Validators.required),

  })
  filterbyName3=[]
  AllUsers4
  search3(filterBye){
    this.branch = this.AllUsers4
    this.filterbyName3 =[]
    for (let index = 0; index < this.branch.length; index++) {
      if(this.branch[index].question !=null )

      
      if(this.branch[index].question.toLowerCase().includes(filterBye.toLowerCase())){
        this.filterbyName3.push(this.branch[index])
      }
      
    }
   this.branch = this.filterbyName3
  }


  ngOnInit() {

    
     this.ApiService.show_country().subscribe(
       res => {

         let resources: any[] = res["data"];
         this.branch=resources;
         this.AllUsers4=resources;

       });
       this.resetForm();

  }

  

   
  onSubmit(formValue) {
 
    
    if (this.Userform.valid) {
 


      
      this.ApiService.insert_country(formValue).subscribe(
        res => {

         this.toastr.success(' ', 'insert successfully');
         this.resetForm();
         this.ngOnInit();
         this.currentPage = "list";  

       });
    
      }else{
        this.toastr.warning('Please fill out all fields correctly', '');

      }
 

  }

  onUpdate(formValue) {

    
    if (this.Userform.valid) {
 


      
      this.ApiService.update_country(formValue,this.taskid).subscribe(
        res => {

         this.toastr.success(' ', 'Update successfully');
         this.resetForm();
         this.ngOnInit();
         this.currentPage = "list";  

       });
    
      }else{
        this.toastr.warning('Please fill out all fields correctly', '');

      }
 



  }
  get formControls() {
    return this.Userform['controls'];
  }


  resetForm() {
    this.Userform.reset();
   
    this.imgSrc = "assets/img/image-placeholder.jpg";
    this.selectedImage = null;
    this.isSubmitted = false;
  }


  showPage(page: string) {
    this.currentPage = page;
    this.ngOnInit()
  }

  deleteTask(id) {

  
     swal.fire({
       title: 'Are you sure?',
       text: "You won't be able to revert this!",
       type: 'warning',
       showCancelButton: true,
       confirmButtonColor: '#3085d6',
       cancelButtonColor: '#d33',
       confirmButtonText: 'Yes, delete it!'
     }).then((result) => {
       if (result.value) {

     this.ApiService.delete_country(id).subscribe(
       res => {
  
         this.ngOnInit();
       });
  

         swal.fire(
           'Deleted!',
           'Your use has been deleted.',
           'success'

           
         )
       }
     })
 
   
  }


  updatetask(page, id) {


    this.currentPage = page;
     this.taskid = id;


     this.ApiService.show_countryByid(id).subscribe(
      res => {

        let resources: any[] = res["data"];
        this.branchD = resources

        this.Userform.setValue({
          a_name: this.branchD['name'],
          E_name: this.branchD['E_name'],

          
        });
      
        
      });

  }
  


  backfun(){

    this._location.back();
  }

  
  

}
