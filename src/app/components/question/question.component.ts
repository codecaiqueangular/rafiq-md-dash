import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../shared/services/api.service';
import swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';
import {Location} from '@angular/common';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {

 
  constructor(public toastr: ToastrService,
    private ApiService: ApiService,
     private cookieService: CookieService, private router: Router, private route: ActivatedRoute , private _location :Location) {

   
     }
  currentPage: string = "list";
  p: number = 1;
  taskid: string;
  imageUrl = environment.imageUrl;

  imgSrc: string ="assets/img/image-placeholder.jpg";
  selectedImage: any = null;
  isSubmitted: boolean;
  branch:any[];
  branchD:any[];
  branchId:any;
  
  Userform = new FormGroup({
    sub_id: new FormControl('',),
    question: new FormControl('', Validators.required),
    E_question: new FormControl('', Validators.required),

  })


  ngOnInit() {
    this.branchId=+this.route.snapshot.paramMap.get('id');

     this.ApiService.show_testByid(this.branchId).subscribe(
       res => {

         let resources: any[] = res["questions"];
         this.branch=resources;
      
       });

  }

  

   
  onSubmit(formValue) {
 
    
    if (this.Userform.valid) {
 

      formValue["sub_id"]= this.branchId ;
     
      this.ApiService.insert_testquestion(formValue).subscribe(
        res => {

         this.toastr.success(' ', 'insert successfully');
         this.resetForm();
         this.ngOnInit();
         this.currentPage = "list";  

       });
    
      }else{
        this.toastr.warning('Please fill out all fields correctly', '');

      }
 

  }


  get formControls() {
    return this.Userform['controls'];
  }



  showPreview(event: any) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => this.imgSrc = e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.selectedImage = event.target.files[0];
    }
    else {
      this.imgSrc ="assets/img/image-placeholder.jpg" ;
      
      this.selectedImage = null;
    }
  }


  resetForm() {
    this.Userform.reset();
   
    this.imgSrc = "assets/img/image-placeholder.jpg";
    this.selectedImage = null;
    this.isSubmitted = false;
  }


  showPage(page: string) {
    this.currentPage = page;
    this.ngOnInit()
  }

  deleteTask(id) {

  
     swal.fire({
       title: 'Are you sure?',
       text: "You won't be able to revert this!",
       type: 'warning',
       showCancelButton: true,
       confirmButtonColor: '#3085d6',
       cancelButtonColor: '#d33',
       confirmButtonText: 'Yes, delete it!'
     }).then((result) => {
       if (result.value) {

     this.ApiService.delete_testquestion(id).subscribe(
       res => {
  
         this.ngOnInit();
       });
  

         swal.fire(
           'Deleted!',
           'Your use has been deleted.',
           'success'

           
         )
       }
     })
 
   
  }

  

  updatetask(page, id) {


    this.currentPage = page;
     this.taskid = id;


     this.ApiService.show_testquestionsByid(id).subscribe(
      res => {

        let resources: any[] = res["data"];
        this.branchD = resources

        this.Userform.setValue({
          sub_id: this.branchId,
          question: this.branchD[0]['question'],
          E_question: this.branchD[0]['E_question'],

        });

      });


  }
  


  onUpdate(formValue) {

    
    if (this.Userform.valid) {
 

      formValue["sub_id"]= this.branchId ;
     
      this.ApiService.update_testquestions(formValue,this.taskid).subscribe(
        res => {

         this.toastr.success(' ', 'Update successfully');
         this.resetForm();
         this.ngOnInit();
         this.currentPage = "list";  

       });
    
      }else{
        this.toastr.warning('Please fill out all fields correctly', '');

      }
 



  }

  backfun(){

    this._location.back();
  }

  
  

}
