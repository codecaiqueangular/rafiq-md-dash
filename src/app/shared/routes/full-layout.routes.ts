import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../shared/auth/auth-guard.service';
import { ProfileComponent } from '../../components/profile/profile.component';
import { UsersComponent } from '../../components/users/users.component';
import { TermsConditioonsComponent } from '../../components/terms-conditioons/terms-conditioons.component';
import { ContactUsComponent } from '../../components/contact-us/contact-us.component';
import { SettingComponent } from '../../components/setting/setting.component';
import { CityComponent } from '../../components/city/city.component';
import { AboutUsComponent } from '../../components/about-us/about-us.component';
import { MangerAcountComponent } from '../../components/manger-acount/manger-acount.component';
import { TestsComponent } from '../../components/tests/tests.component';
import { SpecialistsComponent } from '../../components/specialists/specialists.component';
import { QuestionComponent } from '../../components/question/question.component';
import { AnswersComponent } from '../../components/answers/answers.component';
import { SpectialTypeComponent } from '../../components/spectial-type/spectial-type.component';
import { SpectialQuestionComponent } from '../../components/spectial-question/spectial-question.component';
import { ReservationsComponent } from '../../components/reservations/reservations.component';
import { LanguageComponent } from '../../components/language/language.component';
import { CashComponent } from '../../components/cash/cash.component';
import { ConditionsComponent } from '../../components/conditions/conditions.component';
import { NewsComponent } from '../../components/news/news.component';
import { InstitutionsComponent } from '../../components/institutions/institutions.component';
import { PreviousDealingsComponent } from '../../components/previous-dealings/previous-dealings.component';
import { CoponsComponent } from '../../components/copons/copons.component';
import { AdminsComponent } from '../../components/admins/admins.component';
import { StudentsComponent } from '../../components/users/students/students.component';
import { WlyAlamerComponent } from '../../components/users/wly-alamer/wly-alamer.component';
import { TrainerComponent } from '../../components/users/trainer/trainer.component';
import { ConfirmResComponent } from '../../components/reservations/confirm-res/confirm-res.component';
import { DashComponent } from '../../components/dash/dash.component';
import { BlogComponent } from '../../components/blog/blog.component';
import { CategoryComponent } from '../../components/blog/category/category.component';
import { OfferComponent } from 'app/components/offer/offer.component';
import { HomeComponent } from 'app/components/home/home.component';
import { NotificationsComponent } from 'app/components/notifications/notifications.component';







export const Full_ROUTES: Routes = [
  
  
  
  {
    path: 'chat',
    loadChildren: () => import('../../components/chat/chat.module').then(m => m.ChatModule)
  } ,

  { path: 'dashboard', component: DashComponent, data: { title: 'DashComponent' }, canActivate: [AuthGuard]},
  
   { path: 'profile', component: ProfileComponent, data: { title: 'profile' }, canActivate: [AuthGuard]},
   { path: 'users', component: UsersComponent, data: { title: 'users' }, canActivate: [AuthGuard]},

   { path: 'home_content', component: HomeComponent, data: { title: 'home' }, canActivate: [AuthGuard]},
   { path: 'termsandconditions', component: TermsConditioonsComponent, data: { title: 'terms and conditions' }, canActivate: [AuthGuard]   },
   { path: 'contactUs', component: ContactUsComponent, data: { title: 'contactUs' }, canActivate: [AuthGuard]   },
   { path: 'notifications', component: NotificationsComponent, data: { title: 'notifications' }, canActivate: [AuthGuard]   },

   { path: 'setting', component: SettingComponent, data: { title: 'setting' }, canActivate: [AuthGuard]   },
   { path: 'countries', component: CityComponent, data: { title: 'city' }, canActivate: [AuthGuard]   },
   { path: 'aboutUs', component: AboutUsComponent, data: { title: 'AboutUsComponent' }, canActivate: [AuthGuard]   },
   { path: 'mangerAcount', component: MangerAcountComponent, data: { title: 'MangerAcountComponent' }, canActivate: [AuthGuard]   },
   { path: 'tests', component: TestsComponent, data: { title: 'TestsComponent' }, canActivate: [AuthGuard]   },
   { path: 'specialists', component: SpecialistsComponent, data: { title: 'SpecialistsComponent' }, canActivate: [AuthGuard]   },
   { path: 'question/:id', component: QuestionComponent, data: { title: 'QuestionComponent' }, canActivate: [AuthGuard]   },
   { path: 'answers/:id', component: AnswersComponent, data: { title: 'AnswersComponent' }, canActivate: [AuthGuard]   },
   { path: 'specialType', component: SpectialTypeComponent, data: { title: 'SpectialTypeComponent' }, canActivate: [AuthGuard]   },
   { path: 'specialQuestion/:id', component: SpectialQuestionComponent, data: { title: 'SpectialQuestionComponent' }, canActivate: [AuthGuard]   },
   { path: 'reservations', component: ReservationsComponent, data: { title: 'ReservationsComponent' }, canActivate: [AuthGuard]   },
   { path: 'Offer', component: OfferComponent, data: { title: 'OfferComponent' }, canActivate: [AuthGuard]   },

   { path: 'language', component: LanguageComponent, data: { title: 'LanguageComponent' }, canActivate: [AuthGuard]   },
   { path: 'cash', component: CashComponent, data: { title: 'CashComponent' }, canActivate: [AuthGuard]   },
   { path: 'conditions', component: ConditionsComponent, data: { title: 'ConditionsComponent' }, canActivate: [AuthGuard]   },
   { path: 'news', component: NewsComponent, data: { title: 'NewsComponent' }, canActivate: [AuthGuard]   },
   { path: 'institutions', component: InstitutionsComponent, data: { title: 'InstitutionsComponent' }, canActivate: [AuthGuard]   },
   { path: 'previousDealings/:id', component: PreviousDealingsComponent, data: { title: 'PreviousDealingsComponent' }, canActivate: [AuthGuard]   },
   { path: 'copons', component: CoponsComponent, data: { title: 'CoponsComponent' }, canActivate: [AuthGuard]   },
   { path: 'admins', component: AdminsComponent, data: { title: 'AdminsComponent' }, canActivate: [AuthGuard]   },
   { path: 'students', component: StudentsComponent, data: { title: 'StudentsComponent' }, canActivate: [AuthGuard]   },
   { path: 'wlyAlamer', component: WlyAlamerComponent, data: { title: 'WlyAlamerComponent' }, canActivate: [AuthGuard]   },
   { path: 'trainer', component: TrainerComponent, data: { title: 'TrainerComponent' }, canActivate: [AuthGuard]   },
   { path: 'confirmRes', component: ConfirmResComponent, data: { title: 'ConfirmResComponent' }, canActivate: [AuthGuard]   },
   { path: 'blog/:id', component: BlogComponent, data: { title: 'BlogComponent' }, canActivate: [AuthGuard]   },
   { path: 'category', component: CategoryComponent, data: { title: 'CategoryComponent' }, canActivate: [AuthGuard]   },

   
   

  
];
  