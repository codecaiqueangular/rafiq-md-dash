import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http'
import { Observable, of, } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';

const httpOptions = {
  headers: new HttpHeaders({ 'content': 'application/json' })
}
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  endpoint = environment.apiHost;
  user_id: string;
  token: string;
  lang: string;

  constructor(private http: HttpClient, private cookieService: CookieService) {
    this.token = this.cookieService.get('user_token')
    this.lang = this.cookieService.get('lang')

    console.log(this.token )

  }
  /*************************************login*************************************************/

  count_data(date_from, date_to): Observable<any> {

    return this.http.get(this.endpoint + 'count_data?date_from=' + date_from + '&date_to=' + date_to);
  }
  /*************************************login*************************************************/

  login(body): Observable<any> {
    return this.http.post(this.endpoint + 'admin_login', body)
  }

  update_adminProfile(formValue, image: File): Observable<any> {

    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('first_name', formValue['first_name']);
    formData.append('last_name', formValue['last_name']);
    formData.append('phone', formValue['phone']);
    formData.append('email', formValue['email']);
    formData.append('passwords', formValue['passwords']);
    formData.append('image', image);

    return this.http.post(this.endpoint + 'edit_adminprofile', formData)
  }

  show_adminById(): Observable<any> {
    return this.http.get(this.endpoint + 'show_adminById?user_token=' + this.token);
  }



  /*************************************users*************************************************/

  show_trainer(): Observable<any> {
    this.lang = this.cookieService.get('lang')

    return this.http.get(this.endpoint + 'show_trainer?user_token=' + this.token +'&lang=' + this.lang);
  }

  show_pendingtrainer(): Observable<any> {
    this.lang = this.cookieService.get('lang')

    return this.http.get(this.endpoint + 'show_pendingtrainer?user_token=' + this.token +'&lang=' + this.lang);
  }


  show_students(): Observable<any> {
    this.lang = this.cookieService.get('lang')

    return this.http.get(this.endpoint + 'show_students?user_token=' + this.token +'&lang=' + this.lang);
  }

  show_wlyAlamer(): Observable<any> {
    this.lang = this.cookieService.get('lang')

    return this.http.get(this.endpoint + 'show_wlyAlamer?user_token=' + this.token +'&lang=' + this.lang);
  }

  show_blockedwlyamer(): Observable<any> {
    this.lang = this.cookieService.get('lang')

    return this.http.get(this.endpoint + 'show_blockedwlyamer?user_token=' + this.token +'&lang=' + this.lang);
  }
  show_blockedstudent(): Observable<any> {
    this.lang = this.cookieService.get('lang')

    return this.http.get(this.endpoint + 'show_blockedstudent?user_token=' + this.token +'&lang=' + this.lang);
  }

  show_blockedtrainer(): Observable<any> {
    this.lang = this.cookieService.get('lang')

    return this.http.get(this.endpoint + 'show_blockedtrainer?user_token=' + this.token +'&lang=' + this.lang);
  }
  recharge_wallet(formValue): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('user_id', formValue['user_id']);
    formData.append('cash', formValue['cash']);

    return this.http.post(this.endpoint + 'recharge_wallet', formData)
  }


  block_user(id): Observable<any> {
    this.lang = this.cookieService.get('lang')

    return this.http.get(this.endpoint + 'block_user?user_token=' + this.token + '&user_id=' + id +'&lang=' + this.lang);
  }

  unblock_user(id): Observable<any> {
    this.lang = this.cookieService.get('lang')

    return this.http.get(this.endpoint + 'unblock_user?user_token=' + this.token + '&user_id=' + id +'&lang=' + this.lang);
  }
  /*************************************specialists*************************************************/

  show_specialists(): Observable<any> {
    this.lang = this.cookieService.get('lang')

    return this.http.get(this.endpoint + 'show_specialists?user_token=' + this.token + '&lang=' + this.lang);
  }


  show_specialistsByid(id): Observable<any> {
    this.lang = this.cookieService.get('lang')


    return this.http.get(this.endpoint
      + 'show_specialistsByid?user_token=' + this.token + '&lang=' + this.lang + '&id=' + id + '&lang=' + this.lang);
  }


  delete_specialist(id): Observable<any> {
    return this.http.get(this.endpoint + 'delete_specialist?user_token=' + this.token + '&id=' + id );
  }

  insert_specialists(formValue, image: File): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('name', formValue['name']);
    formData.append('E_name', formValue['E_name']);
    formData.append('image', image);


    return this.http.post(this.endpoint + 'insert_specialists', formData)
  }

  update_specialists(formValue, image: File, id): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('name', formValue['name']);
    formData.append('E_name', formValue['E_name']);
    formData.append('image', image);
    formData.append('id', id);

    return this.http.post(this.endpoint + 'update_specialists', formData)
  }


  /************************************tests**************************************************/

  show_tests(): Observable<any> {
    this.lang = this.cookieService.get('lang')
    return this.http.get(this.endpoint + 'show_tests?user_token=' + this.token + '&lang=' + this.lang);
  }

  insert_test(formValue, image: File): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('name', formValue['name']);
    formData.append('E_name', formValue['E_name']);
    formData.append('image', image);

    return this.http.post(this.endpoint + 'insert_test', formData)
  }

  update_test(formValue, image: File, id): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('name', formValue['name']);
    formData.append('E_name', formValue['E_name']);
    formData.append('image', image);
    formData.append('id', id);

    return this.http.post(this.endpoint + 'update_test', formData)
  }

  delete_test(id): Observable<any> {
    return this.http.get(this.endpoint + 'delete_test?user_token=' + this.token + '&id=' + id);
  }

  show_testByid(test_id): Observable<any> {
    this.lang = this.cookieService.get('lang')

    return this.http.get(this.endpoint
      + 'show_testByid?user_token=' + this.token + '&lang=' + this.lang + '&test_id=' + test_id + '&lang=' + this.lang);
  }

  show_onetest(id): Observable<any> {
    
    return this.http.get(this.endpoint + 'show_onetest?user_token=' + this.token + '&test_id=' + id);
  }

  /******************************************Question type ****************************************************/
  show_questiontype(): Observable<any> {
    this.lang = this.cookieService.get('lang')

    return this.http.get(this.endpoint
      + 'show_questiontype?user_token=' + this.token + '&lang=' + this.lang);
  }

  show_onetype(id): Observable<any> {
    return this.http.get(this.endpoint
      + 'show_onetype?user_token=' + this.token + '&id=' + id);
  }

  show_typeByid(id): Observable<any> {
    return this.http.get(this.endpoint
      + 'show_typeByid?user_token=' + this.token + '&id=' + id);
  }

  delete_type(id): Observable<any> {
    return this.http.get(this.endpoint + 'delete_type?user_token=' + this.token + '&id=' + id);
  }

  insert_type(formValue): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('name', formValue['name']);
    formData.append('E_name', formValue['E_name']);

    return this.http.post(this.endpoint + 'insert_type', formData)
  }

  update_type(formValue, id): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('name', formValue['name']);
    formData.append('E_name', formValue['E_name']);
    formData.append('id', id);

    return this.http.post(this.endpoint + 'update_type', formData)
  }

  /******************************************show question type ****************************************************/
  show_questionByid(id): Observable<any> {
    return this.http.get(this.endpoint
      + 'show_questionByid?user_token=' + this.token + '&id=' + id);
  }

  delete_question(id): Observable<any> {
    return this.http.get(this.endpoint
      + 'delete_question?user_token=' + this.token + '&id=' + id);
  }

  insert_question(formValue): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('question', formValue['question']);
    formData.append('E_question', formValue['E_question']);
    formData.append('answer', formValue['answer']);
    formData.append('E_answer', formValue['E_answer']);
    formData.append('type_id', formValue['type_id']);

    return this.http.post(this.endpoint + 'insert_question', formData)
  }


  update_question(formValue, id): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('question', formValue['question']);
    formData.append('E_question', formValue['E_question']);
    formData.append('answer', formValue['answer']);
    formData.append('E_answer', formValue['E_answer']);
    formData.append('type_id', formValue['type_id']);
    formData.append('id', id);

    return this.http.post(this.endpoint + 'update_question', formData)
  }


  /******************************************test questions****************************************************/
  show_testquestionsByid(question_id): Observable<any> {
    return this.http.get(this.endpoint
      + 'show_testquestionsByid?user_token=' + this.token + '&lang=' + this.lang + '&question_id=' + question_id);
  }

  insert_testquestion(formValue): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('sub_id', formValue['sub_id']);
    formData.append('question', formValue['question']);
    formData.append('E_question', formValue['E_question']);

    return this.http.post(this.endpoint + 'insert_testquestion', formData)
  }


  update_testquestions(formValue, id): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('sub_id', formValue['sub_id']);
    formData.append('question', formValue['question']);
    formData.append('E_question', formValue['E_question']);
    formData.append('id', id);

    return this.http.post(this.endpoint + 'update_testquestions', formData)
  }


  delete_testquestion(id): Observable<any> {
    return this.http.get(this.endpoint + 'delete_testquestion?user_token=' + this.token + '&id=' + id);
  }


  /***********************************Answer**************************************************/
  insert_answer(formValue): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('answer', formValue['answer']);
    formData.append('E_answer', formValue['E_answer']);
    formData.append('question_id', formValue['question_id']);
    formData.append('degree', formValue['degree']);

    return this.http.post(this.endpoint + 'insert_answer', formData)
  }

  update_Answer(formValue, id): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('answer', formValue['answer']);
    formData.append('E_answer', formValue['E_answer']);
    formData.append('id', id);
    formData.append('degree', formValue['degree']);

    return this.http.post(this.endpoint + 'update_Answer', formData)
  }


  delete_Answer(id): Observable<any> {
    return this.http.get(this.endpoint + 'delete_Answer?user_token=' + this.token + '&id=' + id);
  }


  show_answerByid(answer_id): Observable<any> {
    return this.http.get(this.endpoint
      + 'show_answerByid?user_token=' + this.token + '&lang=' + this.lang + '&answer_id=' + answer_id);
  }

  /***********************************country**************************************************/
  show_country(): Observable<any> {
    this.lang = this.cookieService.get('lang')
    
    return this.http.get(this.endpoint
      + 'show_country?user_token=' + this.token  + '&lang=' + this.lang);
  }

  show_countryByid(id): Observable<any> {
    return this.http.get(this.endpoint
      + 'show_countryByid?user_token=' + this.token + '&id=' + id);
  }

  delete_country(id): Observable<any> {
    return this.http.get(this.endpoint + 'delete_country?user_token=' + this.token + '&id=' + id);
  }

  insert_country(formValue): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('a_name', formValue['a_name']);
    formData.append('E_name', formValue['E_name']);


    return this.http.post(this.endpoint + 'insert_country', formData)
  }

  update_country(formValue, id): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('a_name', formValue['a_name']);
    formData.append('E_name', formValue['E_name']);
    formData.append('id', id);


    return this.http.post(this.endpoint + 'update_country', formData)
  }
  /***********************************institutions**************************************************/
  show_institutions(): Observable<any> {
    this.lang = this.cookieService.get('lang')

    return this.http.get(this.endpoint
      + 'show_institutions?user_token=' + this.token + '&lang=' + this.lang);
  }

  show_institutionByid(id): Observable<any> {
    return this.http.get(this.endpoint
      + 'show_institutionByid?user_token=' + this.token + '&id=' + id);
  }

  delete_institutions(id): Observable<any> {
    return this.http.get(this.endpoint + 'delete_institutions?user_token=' + this.token + '&id=' + id);
  }


  insert_institutions(formValue, image: File): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('name', formValue['name']);
    formData.append('E_name', formValue['E_name']);
    formData.append('class_number', formValue['class_number']);
    formData.append('email', formValue['email']);
    formData.append('phone', formValue['phone']);
    formData.append('passwords', formValue['passwords']);
    formData.append('image', image);

    return this.http.post(this.endpoint + 'insert_institutions', formData)
  }



  update_institution(formValue, image: File, id): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('name', formValue['name']);
    formData.append('E_name', formValue['E_name']);
    formData.append('class_number', formValue['class_number']);
    formData.append('email', formValue['email']);
    formData.append('phone', formValue['phone']);
    formData.append('passwords', formValue['passwords']);
    formData.append('image', image);
    formData.append('id', id);

    return this.http.post(this.endpoint + 'update_institution', formData)
  }

  /***********************************Languages**************************************************/

  show_Languages(): Observable<any> {
    this.lang = this.cookieService.get('lang')

    return this.http.get(this.endpoint
      + 'show_Languages?user_token=' + this.token  + '&lang=' + this.lang);
  }

  show_languageByid(id): Observable<any> {
    return this.http.get(this.endpoint
      + 'show_languageByid?user_token=' + this.token + '&id=' + id);
  }

  delete_language(id): Observable<any> {
    return this.http.get(this.endpoint + 'delete_language?user_token=' + this.token + '&id=' + id);
  }

  insert_language(formValue): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('name', formValue['name']);
    formData.append('E_name', formValue['E_name']);


    return this.http.post(this.endpoint + 'insert_language', formData)
  }

  update_language(formValue, id): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('name', formValue['name']);
    formData.append('E_name', formValue['E_name']);
    formData.append('id', id);

    return this.http.post(this.endpoint + 'update_language', formData)
  }

  /***********************************Cash**************************************************/
  show_payments(): Observable<any> {
    this.lang = this.cookieService.get('lang')

    return this.http.get(this.endpoint
      + 'show_payments?user_token=' + this.token  + '&lang=' + this.lang);
  }

  show_paymentByid(id): Observable<any> {
    return this.http.get(this.endpoint
      + 'show_paymentByid?user_token=' + this.token + '&id=' + id);
  }

  delete_payment(id): Observable<any> {
    return this.http.get(this.endpoint + 'delete_payment?user_token=' + this.token + '&id=' + id);
  }

  insert_payment(formValue): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('name', formValue['name']);
    formData.append('E_name', formValue['E_name']);


    return this.http.post(this.endpoint + 'insert_payment', formData)
  }

  update_payment(formValue, id): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('name', formValue['name']);
    formData.append('E_name', formValue['E_name']);
    formData.append('id', id);

    return this.http.post(this.endpoint + 'update_payment', formData)
  }
  /***********************************show_allreservations**************************************************/
  show_allreservations(): Observable<any> {
    this.lang = this.cookieService.get('lang')

    return this.http.get(this.endpoint
      + 'show_allreservations?user_token=' + this.token + '&lang=' + this.lang);
  }

  show_acceptedreservations(): Observable<any> {
    this.lang = this.cookieService.get('lang')

    return this.http.get(this.endpoint
      + 'show_acceptedreservations?user_token=' + this.token + '&lang=' + this.lang);
  }
  show_canceledreservations(): Observable<any> {
    this.lang = this.cookieService.get('lang')

    return this.http.get(this.endpoint
      + 'show_canceledreservations?user_token=' + this.token + '&lang=' + this.lang);
  }
  show_reservationshistory(): Observable<any> {
    this.lang = this.cookieService.get('lang')

    return this.http.get(this.endpoint
      + 'show_reservationshistory?user_token=' + this.token + '&lang=' + this.lang);
  }

  search_reservations(formValue): Observable<any> {
    const formData: FormData = new FormData();
    this.lang = this.cookieService.get('lang')

    formData.append('user_token', this.token);
    formData.append('date_from', formValue['date_from']);
    formData.append('date_to', formValue['date_to']);
    formData.append('lang', this.lang);

    return this.http.post(this.endpoint + 'search_reservations', formData)
  }
  /*************************************news*************************************************/

  show_News(): Observable<any> {
    this.lang = this.cookieService.get('lang')

    return this.http.get(this.endpoint
      + 'show_News?user_token=' + this.token + '&lang=' + this.lang);
  }

  delete_New(id): Observable<any> {
    return this.http.get(this.endpoint + 'delete_New?user_token=' + this.token + '&id=' + id);
  }

  show_Newbyid(id): Observable<any> {
    return this.http.get(this.endpoint + 'show_Newbyid?user_token=' + this.token + '&id=' + id);
  }

  insert_New(formValue, image: File): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('name', formValue['name']);
    formData.append('link', formValue['link']);
    formData.append('image', image);

    return this.http.post(this.endpoint + 'insert_New', formData)
  }

  update_New(formValue, image: File, id): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('name', formValue['name']);
    formData.append('link', formValue['link']);
    formData.append('image', image);
    formData.append('id', id);

    return this.http.post(this.endpoint + 'update_New', formData)
  }

  /*************************************news*************************************************/

  show_about(): Observable<any> {
    this.lang = this.cookieService.get('lang')

    return this.http.get(this.endpoint
      + 'show_about?user_token=' + this.token + '&lang=' + this.lang);
  }

  update_about(formValue, image: File): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('title', formValue['title']);
    formData.append('e_title', formValue['E_title']);
    formData.append('address', formValue['address']);
    formData.append('e_address', formValue['E_address']);
    formData.append('phone', formValue['phone']);
    formData.append('email', formValue['email']);
    formData.append('details', formValue['details']);
    formData.append('e_details', formValue['e_details']);

    formData.append('face_link', formValue['face_link']);
    formData.append('insta_link', formValue['insta_link']);
    formData.append('linked_link', formValue['linked_link']);
    formData.append('twitter_link', formValue['twitter_link']);

    formData.append('logo', image);


    return this.http.post(this.endpoint + 'update_about', formData)
  }


  /*************************************show_policy*************************************************/

  show_policy(): Observable<any> {
    this.lang = this.cookieService.get('lang')

    return this.http.get(this.endpoint + 'show_policy?user_token=' + this.token + '&lang=' + this.lang);
  }

  show_policyByid(): Observable<any> {
    return this.http.get(this.endpoint + 'show_policyByid?user_token=' + this.token);
  }

  show_subjectByid(id): Observable<any> {
    return this.http.get(this.endpoint + 'show_subjectByid?user_token=' + this.token + '&id=' + id);
  }


  update_policy(formValue): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('text', formValue['text']);
    formData.append('E_text', formValue['E_text']);

    return this.http.post(this.endpoint + 'update_policy', formData)
  }

  insert_subject(formValue): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('title', formValue['title']);
    formData.append('E_title', formValue['E_title']);
    formData.append('details', formValue['details']);
    formData.append('E_details', formValue['E_details']);
    return this.http.post(this.endpoint + 'insert_subject', formData)
  }

  update_subject(formValue, id): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('title', formValue['title']);
    formData.append('E_title', formValue['E_title']);
    formData.append('details', formValue['details']);
    formData.append('E_details', formValue['E_details']);
    formData.append('id', id);
    return this.http.post(this.endpoint + 'update_subject', formData)
  }


  delete_subject(id): Observable<any> {
    return this.http.get(this.endpoint + 'delete_subject?user_token=' + this.token + '&id=' + id);
  }


  /*************************************show_conditions*************************************************/

  show_conditions(): Observable<any> {
    this.lang = this.cookieService.get('lang')

    return this.http.get(this.endpoint + 'show_conditions?user_token=' + this.token + '&lang=' + this.lang);
  }

  show_conditionByid(): Observable<any> {
    return this.http.get(this.endpoint + 'show_conditionByid?user_token=' + this.token);
  }

  show_onesubject(id): Observable<any> {
    return this.http.get(this.endpoint + 'show_onesubject?user_token=' + this.token + '&id=' + id);
  }


  update_conditions(formValue): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('text', formValue['text']);
    formData.append('E_text', formValue['E_text']);

    return this.http.post(this.endpoint + 'update_conditions', formData)
  }

  insert_conditionsubject(formValue): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('title', formValue['title']);
    formData.append('E_title', formValue['E_title']);
    formData.append('details', formValue['details']);
    formData.append('E_details', formValue['E_details']);
    return this.http.post(this.endpoint + 'insert_conditionsubject', formData)
  }

  update_conditionsubject(formValue, id): Observable<any> {
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('title', formValue['title']);
    formData.append('E_title', formValue['E_title']);
    formData.append('details', formValue['details']);
    formData.append('E_details', formValue['E_details']);
    formData.append('id', id);
    return this.http.post(this.endpoint + 'update_conditionsubject', formData)
  }


  delete_subjectconditions(id): Observable<any> {
    return this.http.get(this.endpoint + 'delete_subjectconditions?user_token=' + this.token + '&id=' + id);
  }
  /*************************************update_commission*************************************************/

  show_PreviousDealings(id): Observable<any> {
    return this.http.get(this.endpoint + 'show_PreviousDealings?user_token=' + this.token + '&id=' + id);
  }

  update_commission(formValue): Observable<any> {

    console.log(formValue)
    const formData: FormData = new FormData();

    formData.append('user_token', this.token);
    formData.append('comission', formValue['comission']);
    formData.append('id', formValue['user_id']);
    return this.http.post(this.endpoint + 'update_commission', formData)
  }

 /*************************************show_allCopons*************************************************/
 show_allCopons(): Observable<any> {
  return this.http.get(this.endpoint + 'show_allCopons?user_token=' + this.token );
}

show_copon_ByID(id): Observable<any> {
  return this.http.get(this.endpoint + 'show_copon_ByID?user_token=' + this.token + '&id=' + id);
}

delete_copon(id): Observable<any> {
  return this.http.get(this.endpoint + 'delete_copon?user_token=' + this.token + '&id=' + id);
}

add_copon(formValue): Observable<any> {
  const formData: FormData = new FormData();
  formData.append('user_token', this.token);
  formData.append('code', formValue['code']);
  formData.append('value', formValue['value']);
  formData.append('end_date', formValue['end_date']);

  return this.http.post(this.endpoint + 'add_copon', formData)
}

updated_copon(formValue, id): Observable<any> {
  const formData: FormData = new FormData();
  formData.append('user_token', this.token);
  formData.append('code', formValue['code']);
  formData.append('value', formValue['value']);
  formData.append('end_date', formValue['end_date'])
  formData.append('id', id);
  return this.http.post(this.endpoint + 'updated_copon', formData)
}
 /*************************************Admins*************************************************/

 show_admins(): Observable<any> {
 return this.http.get(this.endpoint + 'show_admins?user_token=' + this.token );
}

show_Adminroles(): Observable<any> {
  return this.http.get(this.endpoint + 'show_Adminroles');
 }


show_adminbyid(id): Observable<any> {
 return this.http.get(this.endpoint + 'show_adminbyid?user_token=' + this.token + '&id=' + id);
}

delete_admin(id): Observable<any> {
 return this.http.get(this.endpoint + 'delete_admin?user_token=' + this.token + '&id=' + id);
}


add_admin(formValue): Observable<any> {
  const formData: FormData = new FormData();
  formData.append('user_token', this.token);
  formData.append('first_name', formValue['first_name']);
  formData.append('last_name', formValue['last_name']);
  formData.append('email', formValue['email'])
  formData.append('phone', formValue['phone'])
  formData.append('passwords', formValue['passwords'])
  formData.append('state_id', formValue['state_id'])
  return this.http.post(this.endpoint + 'add_admin', formData)
}


update_admin(formValue,id): Observable<any> {
  const formData: FormData = new FormData();
  formData.append('user_token', this.token);
  formData.append('first_name', formValue['first_name']);
  formData.append('last_name', formValue['last_name']);
  formData.append('email', formValue['email']);
  formData.append('phone', formValue['phone'])
  formData.append('passwords', formValue['passwords']);
  formData.append('state_id', formValue['state_id']);
  formData.append('id', id)

  return this.http.post(this.endpoint + 'update_admin', formData)
}

 /*************************************show_category*************************************************/

 show_category(): Observable<any> {
  this.lang = this.cookieService.get('lang')

  return this.http.get(this.endpoint + 'show_category?user_token=' + this.token  + '&lang=' + this.lang);
 }
 
 show_categorybyid(id): Observable<any> {
  return this.http.get(this.endpoint + 'show_categorybyid?user_token=' + this.token + '&id=' + id);
 }
 
 delete_category(id): Observable<any> {
  return this.http.get(this.endpoint + 'delete_category?user_token=' + this.token + '&id=' + id);
 }

 add_category(formValue): Observable<any> {
  const formData: FormData = new FormData();
  formData.append('user_token', this.token);
  formData.append('name', formValue['name']);
  formData.append('E_name', formValue['E_name']);

  return this.http.post(this.endpoint + 'add_category', formData)
}
update_category(formValue,id): Observable<any> {
  const formData: FormData = new FormData();
  formData.append('user_token', this.token);
  formData.append('name', formValue['name']);
  formData.append('E_name', formValue['E_name']);
  formData.append('id', id)

  return this.http.post(this.endpoint + 'update_category', formData)
}

 /*************************************show_posts*************************************************/
 
 show_posts(category_id): Observable<any> {
  this.lang = this.cookieService.get('lang')

  return this.http.get(this.endpoint + 'show_posts?user_token=' + this.token 
  + '&category_id=' + category_id + '&lang=' + this.lang);
 }
 
 show_postbyid(id): Observable<any> {
  return this.http.get(this.endpoint + 'show_postbyid?user_token=' + this.token + '&id=' + id);
 }

 delete_post(id): Observable<any> {
  return this.http.get(this.endpoint + 'delete_post?user_token=' + this.token + '&id=' + id);
 }

 add_post(formValue ,image :File): Observable<any> {
  const formData: FormData = new FormData();
  formData.append('user_token', this.token);
  formData.append('title', formValue['title']);
  formData.append('E_title', formValue['E_title']);
  formData.append('description', formValue['description']);
  formData.append('E_description', formValue['E_description']);
  formData.append('details', formValue['details']);
  formData.append('E_details', formValue['E_details']);
  formData.append('arrange', formValue['arrange']);
  formData.append('category_id', formValue['category_id']);
  formData.append('image', image)

  return this.http.post(this.endpoint + 'add_post', formData)
}

update_post(formValue ,image :File,id): Observable<any> {
  const formData: FormData = new FormData();
  formData.append('user_token', this.token);
  formData.append('title', formValue['title']);
  formData.append('E_title', formValue['E_title']);
  formData.append('description', formValue['description']);
  formData.append('E_description', formValue['E_description']);
  formData.append('details', formValue['details']);
  formData.append('E_details', formValue['E_details']);
  formData.append('arrange', formValue['arrange']);
  formData.append('category_id', formValue['category_id']);
  formData.append('image', image)
  formData.append('id', id)

  return this.http.post(this.endpoint + 'update_post', formData)
}



/************************************ show_dashE3lan *****************************************/


show_dashE3lan(): Observable<any> {
  this.lang = this.cookieService.get('lang')

  return this.http.get(this.endpoint + 'show_dashE3lan?user_token=' + this.token 
  + '&lang=' + this.lang);
 }


 update_E3lan(formValue ,image :File): Observable<any> {
  const formData: FormData = new FormData();
  formData.append('user_token', this.token);
  formData.append('text', formValue['text']);
  formData.append('type', 'image');
  formData.append('link',formValue['link'] );
  formData.append('image', image)
 /*  formData.append('type', type) */

  return this.http.post(this.endpoint + 'update_E3lan', formData)
}



/******************************************************** home *************************************/



show_homeData(): Observable<any> {
  this.lang = this.cookieService.get('lang')

  return this.http.get(this.endpoint + 'show_homeData?user_token=' + this.token 
  + '&lang=' + this.lang);
 }


 update_homeData(formValue ,image :File): Observable<any> {
  const formData: FormData = new FormData();
  formData.append('user_token', this.token);
  formData.append('title', formValue['title']);
  formData.append('E_title', formValue['E_title']);
  formData.append('description', formValue['description']);
  formData.append('E_description', formValue['E_description']);

 /*  formData.append('type', type) */

  return this.http.post(this.endpoint + 'update_homeData', formData)
}


show_alladminRoles(id): Observable<any> {
  this.lang = this.cookieService.get('lang')

  return this.http.get(this.endpoint + 'show_alladminRoles?user_token=' + this.token 
  + '&user_id=' + id + '&lang=' + this.lang);
 }


 add_adminrole(formValue ,user_id): Observable<any> {
  const formData: FormData = new FormData();
  formData.append('user_token', this.token);

  formData.append('role_id', formValue['role_id']);
  formData.append('user_id', user_id);

 /*  formData.append('type', type) */

  return this.http.post(this.endpoint + 'add_adminrole', formData)
}

/*  
 show_Adminroles(): Observable<any> {
  this.lang = this.cookieService.get('lang')

  return this.http.get(this.endpoint + 'show_Adminroles');
 }
  */

 show_steps(): Observable<any> {
  this.lang = this.cookieService.get('lang')

  return this.http.get(this.endpoint + 'show_steps?user_token=' + this.token 
  + '&lang=' + this.lang);
 }


 
 update_steps(formValue, image: File, id): Observable<any> {
  const formData: FormData = new FormData();

  formData.append('user_token', this.token);
  formData.append('title', formValue['title']);
  formData.append('E_title', formValue['E_title']);
  formData.append('description', formValue['description']);
  formData.append('E_description', formValue['E_description']);
  formData.append('video', formValue['video']);
  
  formData.append('image', image);
  formData.append('id', id);

  return this.http.post(this.endpoint + 'update_steps', formData)
}



update_steps_vedio(formValue, vedio: File, id): Observable<any> {
  const formData: FormData = new FormData();

  formData.append('user_token', this.token);
  formData.append('title', formValue['title']);
  formData.append('E_title', formValue['E_title']);
  formData.append('description', formValue['description']);
  formData.append('E_description', formValue['E_description']);
  formData.append('video', vedio);
  
  formData.append('image', null);
  formData.append('id', id);

  return this.http.post(this.endpoint + 'update_steps', formData)
}

show_stepbyid(id): Observable<any> {
  return this.http.get(this.endpoint + 'show_stepbyid?user_token=' + this.token + '&id=' + id);
 }


 acceptReservation(id): Observable<any> {
  return this.http.get(this.endpoint + 'acceptReservation?' +'reservation_id=' + id);
 }



 
 send_studentNotification(formValue ): Observable<any> {
  const formData: FormData = new FormData();

  formData.append('user_token', this.token);
  formData.append('title', formValue['title']);
  formData.append('body', formValue['body']);
  formData.append('title_ar', formValue['title_ar']);
  formData.append('body_ar', formValue['body_ar']);
  

  return this.http.post(this.endpoint + 'send_studentNotification', formData)
}


send_parentNotification(formValue ): Observable<any> {
  const formData: FormData = new FormData();

  formData.append('user_token', this.token);
  formData.append('title', formValue['title']);
  formData.append('body', formValue['body']);
  formData.append('title_ar', formValue['title_ar']);
  formData.append('body_ar', formValue['body_ar']);
  

  return this.http.post(this.endpoint + 'send_parentNotification', formData)
}


send_trainerNotification(formValue ): Observable<any> {
  const formData: FormData = new FormData();

  formData.append('user_token', this.token);
  formData.append('title', formValue['title']);
  formData.append('body', formValue['body']);
  formData.append('title_ar', formValue['title_ar']);
  formData.append('body_ar', formValue['body_ar']);
  

  return this.http.post(this.endpoint + 'send_trainerNotification', formData)
}


send_specialNotification(formValue ,user_id): Observable<any> {
  const formData: FormData = new FormData();

  formData.append('user_token', this.token);
  formData.append('title', formValue['title']);
  formData.append('body', formValue['body']);
  formData.append('user_id', user_id);
  formData.append('title_ar', formValue['title_ar']);
  formData.append('body_ar', formValue['body_ar']);

  

  return this.http.post(this.endpoint + 'send_specialNotification', formData)
}


show_all_usersss(): Observable<any> {
  return this.http.get(this.endpoint + 'show_all_users?user_token=' + this.token );
 }

}


