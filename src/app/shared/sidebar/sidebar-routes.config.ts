import { RouteInfo } from './sidebar.metadata';

//Sidebar menu Routes and data
export const ROUTES: RouteInfo[] = [

    
    { path: '/dashboard', title: 'Dashboard', icon: 'ft-home', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    {
        path: '', title: 'Users', icon: 'ft-users', class: 'has-sub', badge: '', badgeClass: '', isExternalLink: false,
        submenu: [

            { path: '/students', title: 'Students', icon: 'ft-users', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
            { path: '/trainer', title: 'Trainer', icon: 'ft-users', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
            { path: '/wlyAlamer', title: 'WlyAlamer', icon: 'ft-users', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },

        ]
    },
   
    { path: '/specialists', title: 'Speciallists', icon: 'icon-users', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    { path: '/home_content', title: 'home', icon: 'ft-home', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    { path: '/notifications', title: 'Notifications', icon: 'icon-puzzle', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },

    { path: '/tests', title: 'Tests', icon: 'icon-puzzle', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    { path: '/specialType', title: 'SpecialQuestions', icon: 'icon-question', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    { path: '/institutions', title: 'Institutions', icon: 'icon-home', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    { path: '/admins', title: 'Admins', icon: 'icon-users', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    { path: '/Offer', title: 'Offer', icon: 'icon-envelope-letter', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },

    { path: '/reservations', title: 'All Reservations', icon: 'ft-clipboard', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    
    
/*     {

        path: '', title: 'Reservations', icon: 'ft-clipboard', class: 'has-sub', badge: '', badgeClass: '', isExternalLink: false,
        submenu: [

            { path: '/reservations', title: 'All Reservations', icon: 'ft-clipboard', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
            // { path: '/waitingRes', title: 'waiting Reservations', icon: 'ft-clipboard', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
           { path: '/confirmRes', title: 'confirmed Resv', icon: 'icon-like', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
 
        ]
    }, */

    { path: '/language', title: 'language', icon: 'icon-flag', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    { path: '/countries', title: 'Countries', icon: 'icon-pointer', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    { path: '/cash', title: 'Payment', icon: 'icon-wallet', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    { path: '/news', title: 'News', icon: 'icon-globe', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    { path: '/copons', title: 'Copons', icon: 'icon-present', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    { path: '/category', title: 'blog', icon: 'icon-speech', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },

    // { path: '/contactUs', title: 'Contact Us Box', icon: 'icon-envelope-letter', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    { path: '/termsandconditions', title: 'Privacy Policy', icon: 'icon-directions', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    { path: '/conditions', title: 'Conditions & Rules', icon: 'icon-directions', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    { path: '/aboutUs', title: 'About Us', icon: 'icon-info', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },



];
